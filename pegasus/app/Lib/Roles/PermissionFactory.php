<?php

namespace App\Lib\Roles;

use App\Lib\Roles\Permission;

class PermissionFactory
{

    /**
     * Returns collection of all roles and their permissions (from config)
     * @return collection
     */
    public static function getGeneralRoles()
    {
        return collect(config('roles'));
    }

    /**
     * Returns collection of all thread related roles and their permissions (from config)
     * @return collection
     */
    public static function getThreadRoles()
    {
        return collect(config('thread_roles'));
    }

    /**
     * Makes an instace of permission
     * @param  string $name name of the permission
     * @return Permission instance of Permission class
     */
    public static function makeGeneral($name)
    {
        return self::make('general', $name);
    }

    /**
     * Makes a collection of permissions from their names
     * @param  array $permissions array of permission names
     * @return collection collection of Permissions
     */
    public static function makeManyGeneral($permissions)
    {
        return self::makeMany('general', $permissions);
    }

    /**
     * Makes an instace of thread permission
     * @param  string $name name of the permission
     * @return Permission instance of Permission class
     */
    public static function makeThread($name)
    {
        return self::make('thread', $name);
    }

    /**
     * Makes a collection of thread permissions from their names
     * @param  array $permissions array of permission names
     * @return collection collection of Permissions
     */
    public static function makeManyThread($permissions)
    {
        return self::makeMany('thread', $permissions);
    }

    private static function make($type, $name)
    {
        if ($type === 'thread') {
            $roles = self::getThreadRoles();
        } else if ($type === 'general') {
            $roles = self::getGeneralRoles();
        } else {
            return null;
        }
        // Filter only those roles that have the permission
        // and return the keys only (only role name, without sub array of permissions)
        $roles = $roles->filter(function ($i, $k) use ($name) {
            return in_array($name, $i);
        })->keys();

        return new Permission($name, $roles);
    }

    private static function makeMany($type, $permissions)
    {
        $tmp = collect();
        if ($permissions === null) {
            return $tmp;
        }
        foreach ($permissions as $permission) {
            $p = self::make($type, $permission);
            if ($p instanceof Permission) {
                $tmp->push(self::make($type, $p));
            }
        }
        return $tmp;
    }
}
