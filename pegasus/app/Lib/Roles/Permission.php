<?php

namespace App\Lib\Roles;

use Illuminate\Support\Collection;

class Permission
{

    protected $name;
    protected $roles;

    // Permission should be initiated with PermissionFactory
    public function __construct($name, $roles)
    {
        $this->name = $name;
        $this->roles = $roles;
    }

    /**
     * Return if permissions has given role
     * @param  string | UserRole $role asked role
     * @return bool if permission belongs to role
     */
    public function belongsToRole($role)
    {
        if ($role instanceof UserRole) {
            $role = $role->getName();
        }
        return $this->getRoles()->search(function ($i, $k) use ($role) {
            return $i == $role;
        }) !== false;
    }

    // Getters and setters
    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getRoles()
    {
        if (($this->roles instanceof Collection) === false) {
            $this->roles = collect($this->roles);
        }
        return $this->roles;
    }

    public function __toString()
    {
        return (string)$this->name;
    }
}
