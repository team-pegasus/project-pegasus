<?php

namespace App\Lib\Roles;

use Illuminate\Support\Collection;

class Role
{

    protected $name;
    protected $permissions;

    // Role should be initiated with RoleFactory
    public function __construct($name, $permissions)
    {
        $this->name = $name;
        $this->permissions = $permissions;
    }

    /**
     * Returns if role has given permission
     * @param  string  $permission Permission name
     * @return boolean             returns if role has the permission
     */
    public function hasPermission($permission)
    {
        return $this->getPermissions()->search(function ($i, $k) use ($permission) {
            return $i == $permission;
        }) !== false;
    }

    // Getters and setters
    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    // Returns a collection of this role's permissions
    public function getPermissions()
    {
        if (($this->permissions instanceof Collection) === false) {
            $this->permissions = collect($this->permissions);
        }
        return $this->permissions;
    }

    public function can($permission)
    {
        return $this->hasPermission($permission);
    }

    public function __toString()
    {
        return $this->name ? $this->name : "";
    }
}
