<?php

namespace App\Lib\Roles;

use App\Lib\Roles\Role;
use App\Lib\Roles\PermissionFactory;

class RoleFactory
{

    /**
     * Returns collection of all thread roles and their permissions (from config)
     * @return collection
     */
    public static function getThreadRoles()
    {
        return collect(config('thread_roles'));
    }

    /**
     * Returns role's permissions
     * @return collection
     */
    public static function getThreadRolePermissions($name)
    {
        return PermissionFactory::makeManyThread(config('thread_roles.' . $name));
    }

    /**
     * Makes an instace of role
     * @param  string $name name of the role
     * @return Role         instance of Role class
     */
    public static function makeThread($name)
    {
        return new Role($name, (self::getThreadRolePermissions($name)));
    }
}
