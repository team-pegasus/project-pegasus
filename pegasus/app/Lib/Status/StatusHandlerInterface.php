<?php

namespace App\Lib\Status;

interface StatusHandlerInterface
{
    // Success messages handling

    /**
     * Adds a new success message to the response
     * @param string $key  Key of the message
     * @param string $text Message content
     */
    public function addSuccess($key, $text);

    /**
     * Returns existing success MessageBag from response or an empty one
     * @return MessageBag
     */
    public function getSuccesses();

    // Error messages handling

    /**
     * Adds a new error message to the response
     * @param [type] $key  [description]
     * @param [type] $text [description]
     */
    public function addError($key, $text);

    /**
     * Returns existing error MessageBag from response or an empty one
     * @return MessageBag
     */
    public function getErrors();
}
