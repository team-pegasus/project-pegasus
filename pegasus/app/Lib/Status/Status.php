<?php

namespace App\Lib\Status;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class Status implements StatusHandlerInterface
{
    private $request;
    private $errorBag;
    private $successBag;

    // Types
    const ERR_NAME = 'errors';
    const SUCCESS_NAME = 'success';

    public function __construct(Request $request)
    {
        $this->request      = $request;
        $this->successBag   = $this->getSuccesses();
        $this->errorBag     = $this->getErrors();
    }

    public function addSuccess($key, $text)
    {
        $this->addToBag(self::SUCCESS_NAME, $key, $text);
    }

    public function getSuccesses()
    {
        return $this->getBag(self::SUCCESS_NAME);
    }

    public function addError($key, $text)
    {
        $this->addToBag(self::ERR_NAME, $key, $text);
    }

    public function getErrors()
    {
        return $this->getBag(self::ERR_NAME);
    }

    /**
     * Adds new message to a status message bag
     * @param string $type One of the constants defined at the top
     * @param string $key  Key of the message
     * @param string $text Text of the message
     */
    private function addToBag($type, $key, $text)
    {
        $bag = $this->getBag($type);
        $bag->add($key, $text);
        $this->request->session()->flash($type, $bag);
        return $bag;
    }

    /**
     * Gets a message bag from session or creates a new one
     * @param  string     $type One of the constants defined at the top
     * @return MessageBag       Message bag from session or a new one
     */
    private function getBag($type)
    {
        try {
            $session = $this->request->session();
        } catch (\Exception $e) {
            return new MessageBag();
        }

        if ($session->has($type) && $session->get($type) instanceof MessageBag) {
            return $session->get($type);
        } else {
            return new MessageBag();
        }
    }
}
