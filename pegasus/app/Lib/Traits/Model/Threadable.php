<?php

namespace App\Lib\Traits\Model;

use App\Models\Messaging\Thread;

trait Threadable
{

    public function thread()
    {
        return $this->morphOne(Thread::class, 'threadable');
    }
}
