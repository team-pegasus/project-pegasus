<?php

namespace App\Lib\Traits\Model;

use App\Models\Categories\Language;

trait Languagable
{
    public function languages()
    {
        return $this->morphToMany(Language::class, 'languagable');
    }
}
