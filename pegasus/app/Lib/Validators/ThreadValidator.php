<?php

namespace App\Lib\Validators;

use DB;
use Validator;
use App\Repos\ThreadRepository;
use App\Models\Messaging\Thread;
use Illuminate\Support\MessageBag;

class ThreadValidator
{

    // Repository for threads
    protected $repo;

    // Instance of errorbag that contains all the errors
    protected $errors;

    public function __construct(MessageBag $errors, ThreadRepository $repo)
    {
        $this->repo = $repo;
        $this->errors = $errors;
    }

    /**
     * Validates whether the user with $attributes can be created
     * Checks uniquness of attributes and stuff
     * @param  array $attributes Array of attributes
     * @param  array $userIds Array of participant ids
     * @param  Model|null $morphTo Model the thread morhps to (or null for private)
     * @return true|ErrorBag     true on success, ErorBag on fail
     */
    public function create($attributes, $userIds = [], $morphTo = null)
    {
        $baseRules = [
            'last_message' => 'dateOrNull',
        ];

        if ($morphTo === null) {
            if (count($userIds) !== 2) {
                $this->errors->add('private', 'Expects 2 users when creating a private thread');
                return $this->errors;
            }
            $existing = $this->repo->findPrivateByUserIds($userIds[0], $userIds[1]);

            if ($existing !== null) {
                $this->errors->add('unique', 'Private thread already exists');
                return $this->errors;
            }
        }

        $validator = Validator::make($attributes, $baseRules);
        return $validator->fails() ? $validator->errors() : true;
    }

    public function addUserToThread($threadId, $userId)
    {
        $thread = Thread::private()->find($threadId);
        if ($thread !== null) {
            $this->errors->add('private', 'The thread is private');
            return $this->errors;
        }

        // Check if user already is/was in thread
        $existing = DB::table('thread_user')
            ->where('thread_id', $threadId)
            ->where('user_id', $userId)
            ->whereNull('left')
            ->first();


        if ($existing !== null) {
            $this->errors->add('unique', 'User already in the thread');
            return $this->errors;
        }

        return true;
    }
}
