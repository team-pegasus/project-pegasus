<?php

namespace App\Lib\Validators;

use Validator;
use App\Models\Messaging\Message;
use Illuminate\Support\MessageBag;

class ProjectValidator
{

    // Instance of errorbag that contains all the errors
    protected $errors;

    public function __construct(MessageBag $errors)
    {
        $this->errors = $errors;
    }

    public function create($attributes)
    {
        $baseRules = [
            'name' => 'required|min:6',
            'description' => 'required|min:200|max:400',
            'content' => 'required',
        ];

        $validator = Validator::make($attributes, $baseRules);
        return $validator->fails() ? $validator->errors() : true;
    }

    public function update($attributes)
    {
        $baseRules = [];
        if (isset($attributes['name'])) {
            $baseRules['name'] = 'required|min:6';
        }
        if (isset($attributes['description'])) {
            $baseRules['description'] = 'required|min:200|max:400';
        }
        if (isset($attributes['content'])) {
            $baseRules['content'] = 'required';
        }

        $validator = Validator::make($attributes, $baseRules);
        return $validator->fails() ? $validator->errors() : true;
    }
}
