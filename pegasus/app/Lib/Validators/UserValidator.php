<?php

namespace App\Lib\Validators;

use App\Models\Users\User;
use Illuminate\Support\MessageBag;

class UserValidator
{

    // Instance of errorbag that contains all the errors
    protected $errors;

    protected $providers = ['github_id', 'google_id', 'facebook_id', 'bitbucket_id', 'twitter_id'];

    public function __construct(MessageBag $errors)
    {
        $this->errors = $errors;
    }

    /**
     * Validates whether the user with $attributes can be created
     * Checks uniquness of attributes and stuff
     * @param  array $attributes Array of attributes
     * @return true|ErrorBag     true on success, ErorBag on fail
     */
    public function create($attributes)
    {
        $baseRules = [
            'username'  => 'required|unique:users,username',
            'email'     => 'required|email|unique:users,email',
            'github_id'     => 'uniqueOrNull:users,github_id',
            'google_id'     => 'uniqueOrNull:users,google_id',
            'facebook_id'   => 'uniqueOrNull:users,facebook_id',
            'twitter_id'    => 'uniqueOrNull:users,twitter_id',
            'bitbucket_id'  => 'uniqueOrNull:users,bitbucket_id',
        ];

        // If user is regestered through social network, they don't need the username
        foreach ($this->providers as $provider) {
            if (isset($attributes[$provider])) {
                unset($baseRules['username']);
                break;
            }
        }

        $validator = \Validator::make($attributes, $baseRules);
        return $validator->fails() ? $validator->errors() : true;
    }

    public function update(array $current, array $new)
    {
        $baseRules = [
            'username'  => 'notSet',
            'email'     => 'notSet',
            'github_id'     => 'notSet',
            'google_id'     => 'notSet',
            'facebook_id'   => 'notSet',
            'twitter_id'    => 'notSet',
            'bitbucket_id'  => 'notSet',
        ];

        // If username is not set on current user, add uniqueOrNull rule to
        // the rule array (overwrite not set)
        if (empty($current['username'])) {
            $baseRules['username'] = 'uniqueOrNull:users,username';
        }

        // Do same for all providers
        foreach ($this->providers as $provider) {
            if (empty($current[$provider])) {
                $baseRules[$provider] = 'uniqueOrNull:users,' . $provider;
            } else if (isset($current[$provider]) &&
                isset($new[$provider]) &&
                $current[$provider] == $new[$provider]) {
                unset($baseRules[$provider]);
            }
        }

        $validator = \Validator::make($new, $baseRules);
        return $validator->fails() ? $validator->errors() : true;
    }
}
