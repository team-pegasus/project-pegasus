<?php

namespace App\Lib\Validators;

use DB;
use Validator;
use App\Models\Messaging\Message;
use Illuminate\Support\MessageBag;

class MessageValidator
{

    // Instance of errorbag that contains all the errors
    protected $errors;

    public function __construct(MessageBag $errors)
    {
        $this->errors = $errors;
    }

    public function create($attributes)
    {
        $baseRules = [
            'content' => 'required',
        ];

        $validator = Validator::make($attributes, $baseRules);
        return $validator->fails() ? $validator->errors() : true;
    }
}
