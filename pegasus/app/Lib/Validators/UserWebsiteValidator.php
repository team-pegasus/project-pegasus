<?php

namespace App\Lib\Validators;

use App\Models\Users\User;
use Illuminate\Validation\Rule;
use App\Repos\UserWebsiteRepository;
use Illuminate\Support\MessageBag;

class UserWebsiteValidator
{

    // Instance of errorbag that contains all the errors
    protected $errors;

    // Instance of UserWebsiteRepository
    protected $repo;

    public function __construct(MessageBag $errors, UserWebsiteRepository $repo)
    {
        $this->errors = $errors;
        $this->repo = $repo;
    }

    /**
     * Validates whether the user with $attributes can be created
     * Checks uniquness of attributes and stuff
     * @param  array $attributes Array of attributes
     * @return true|ErrorBag     true on success, ErorBag on fail
     */
    public function create($attributes)
    {
        $baseRules = [
            'user_id'  => 'required',
            'website_id' => 'required',
            'url' => 'required'
        ];

        // This is just a quick way of adding 'website_id' error
        // response, if the primary key is already taken
        if (isset($attributes['user_id']) && isset($attributes['website_id'])) {
            if ($this->repo->findByIds($attributes['user_id'], $attributes['website_id']) !== null) {
                $baseRules['website_id'] = 'required|unique:user_websites,website_id';
            }
        }

        $validator = \Validator::make($attributes, $baseRules);
        return $validator->fails() ? $validator->errors() : true;
    }
}
