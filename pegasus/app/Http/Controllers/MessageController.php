<?php

namespace App\Http\Controllers;

use Gate;
use Illuminate\Http\Request;
use App\Repos\Contracts\MessageRepositoryInterface;
use App\Updaters\Contracts\MessageUpdaterInterface;

class MessageController extends Controller
{
    // UserWebsite repository
    protected $repo;
    // UserWebsite updater and creator
    protected $updater;

    public function __construct(MessageRepositoryInterface $repo, MessageUpdaterInterface $updater)
    {
        $this->repo = $repo;
        $this->updater = $updater;
    }

    public function create(Request $request, $threadId)
    {
        return response()->json(
            $this->updater->create(
                [
                    'content' => $request->input('text')
                ],
                auth()->user()->id,
                $threadId
            )
        );
    }
}
