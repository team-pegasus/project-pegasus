<?php

namespace App\Http\Controllers;

use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Repos\Contracts\ProjectRepositoryInterface;
use App\Updaters\Contracts\ProjectUpdaterInterface;

class ProjectController extends Controller
{
    // UserWebsite repository
    protected $repo;
    // UserWebsite updater and creator
    protected $updater;

    public function __construct(ProjectRepositoryInterface $repo, ProjectUpdaterInterface $updater)
    {
        $this->repo = $repo;
        $this->updater = $updater;
    }

    public function index(Request $request)
    {
        return response()->json($this->repo->getPaginated());
    }

    public function show(Request $request, $id)
    {
        $project = $this->repo->findById($id);
        if ($project === null) {
            abort(404);
        }
        return response()->json($project);
    }

    public function create(Request $request)
    {
        Gate::authorize('projects.create', auth()->user());
        $result = $this->updater->create($request->input(), auth()->user()->id);
        if ($result instanceof MessageBag) {
            return response()->json([
                'errors' => $result->toArray(),
                'message' => 'The given data was invalid.',
            ], 422);
        }
        return response()->json($result);
    }

    public function update(Request $request, $projId)
    {
        $proj = $this->repo->findById($projId);
        if ($proj === null) {
            abort(404);
        }
        Gate::authorize('projects.update', $proj, auth()->user());
        $result = $this->updater->updateById($proj, $request->input());
        if ($result instanceof MessageBag) {
            return response()->json([
                'errors' => $result->toArray(),
                'message' => 'The given data was invalid.',
            ], 422);
        }
        return response()->json($result);
    }
}
