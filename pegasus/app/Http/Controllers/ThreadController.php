<?php

namespace App\Http\Controllers;

use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Repos\Contracts\ThreadRepositoryInterface;
use App\Updaters\Contracts\ThreadUpdaterInterface;

class ThreadController extends Controller
{

    // UserWebsite repository
    protected $repo;

    public function __construct(ThreadRepositoryInterface $repo, ThreadUpdaterInterface $updater)
    {
        $this->repo = $repo;
        $this->updater = $updater;
    }

    /**
     * Show all user's threads
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Gate::authorize('threads.index', auth()->user());
        return response()->json($this->repo->getByUserId(auth()->user()->id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('threads.create', auth()->user());
        $type = $request->input('type');
        if ($type == 'USER') {
            $morphTo = null;
            // If a private thread already exists
            // with those 2 users in it, return it
            $thread = $this->repo->findPrivateByUserIds($request->input('id'), auth()->user()->id);
            if ($thread !== null) {
                return response()->json($thread);
            }
        } else {
            abort(400);
        }
        $result = $this->updater->create(
            $request->input(),
            [
                auth()->user()->id,
                $request->input('id'),
            ],
            $morphTo
        );
        if ($result instanceof MessageBag) {
            return response()->json($result, 400);
        }
        return response()->json($result);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $userId, $websiteId)
    {
        $thread = $this->repo->findById($websiteId);
        if ($thread === null) {
            abort(404);
        }
        Gate::authorize('threads.show', auth()->user(), $thread);
        return response()->json($thread);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    public function update(Request $request, $userId, $websiteId)
    {
        Gate::authorize('threads.update', auth()->user(), $userId);
        $uw = $this->repo->($userId, $websiteId);
        if ($uw === null) {
            abort(404);
        }
        return response()->json($this->user->update($id, $request->input()));
    }
     */
    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
    public function destroy(Request $request, $userId, $websiteId)
    {
        Gate::authorize('threads.destroy', auth()->user(), $userId);
        $uw = $this->repo->findByIds($userId, $websiteId);
        if ($uw === null) {
            abort(404);
        }
        return response()->json($this->user->deleteByIds($userId, $websiteIds));
    }
     */
}
