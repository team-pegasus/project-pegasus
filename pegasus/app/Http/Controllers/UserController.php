<?php

namespace App\Http\Controllers;

use Gate;
use App\User;
use App\UserRole;
use Illuminate\Http\Request;
use App\Repos\Contracts\UserRepositoryInterface;
use App\Updaters\Contracts\UserUpdaterInterface;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{

    // User repository
    protected $user;
    // Updates/creates users in db
    protected $updater;

    public function __construct(UserRepositoryInterface $user, UserUpdaterInterface $updater)
    {
        $this->user = $user;
        $this->updater = $updater;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Gate::authorize('users.index', auth()->user());
        return response()->json($this->user->getPaginated(10, $request->input('page', null)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('users.create');
        return response()->json($this->updater->create($request->input()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($userId)
    {
        $user = $this->user->findById($userId);
        if ($user === null) {
            abort(404);
        }
        return response()->json($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->user->findById($id);
        if ($user === null) {
            abort(404);
        }
        Gate::authorize('users.update', auth()->user(), $user);
        return response()->json($this->updater->update($id, $request->input()));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
