<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\Access\Gate;
use App\Repos\Contracts\UserWebsiteRepositoryInterface;

class UserWebsiteController extends Controller
{

    // UserWebsite repository
    protected $repo;

    // Gate for policies
    protected $gate;

    public function __construct(UserWebsiteRepositoryInterface $repo, Gate $gate)
    {
        $this->repo = $repo;
        $this->gate = $gate;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->gate->authorize('userwebsites.create', auth()->user(), $request()->input('user_id'));
        return response()->json($this->repo->create($request->input()));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $userId, $websiteId)
    {
        $this->gate->authorize('userwebsites.show', auth()->user(), $userId);
        $uw = $this->repo->findByIds($userId, $websiteId);
        if ($uw === null) {
            abort(404);
        }
        return response()->json($uw);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $userId, $websiteId)
    {
        $this->gate->authorize('users.update', auth()->user(), $userId);
        $uw = $this->repo->findByIds($userId, $websiteId);
        if ($uw === null) {
            abort(404);
        }
        return response()->json($this->user->update($id, $request->input()));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $userId, $websiteId)
    {
        $this->gate->authorize('userswebsites.destroy', auth()->user(), $userId);
        $uw = $this->repo->findByIds($userId, $websiteId);
        if ($uw === null) {
            abort(404);
        }
        return response()->json($this->user->deleteByIds($userId, $websiteIds));
    }
}
