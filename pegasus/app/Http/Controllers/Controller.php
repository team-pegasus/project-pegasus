<?php

namespace App\Http\Controllers;

use App\Lib\Status\StatusHandlerInterface;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $status;

    public function __construct()
    {
        $this->status = app()->make(StatusHandlerInterface::class);
    }

    public function setStatus(StatusHandlerInterface $status)
    {
        $this->status = $status;
    }
}
