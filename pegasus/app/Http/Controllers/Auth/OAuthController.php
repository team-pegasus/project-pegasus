<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Socialite;
use App\Repos\Contracts\UserRepositoryInterface;
use App\Updaters\Contracts\UserUpdaterInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\PostLoginRequest;
use App\Http\Requests\Auth\PostRegisterRequest;

class OAuthController extends Controller
{
    private $user;

    private $updater;

    public function __construct(UserRepositoryInterface $user, UserUpdaterInterface $updater)
    {
        parent::__construct();

        $this->user = $user;
        $this->updater = $updater;
    }

    // Socialite

    /**
     * Redirect the user to the social authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        if ($provider == 'google') {
            // Only request the info we need
            $scopes = [
                'https://www.googleapis.com/auth/userinfo.email'
            ];
            return Socialite::driver($provider)->scopes($scopes)->redirect();
        }
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from social platform.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();
        } catch (Exception $e) {
            return redirect(action('AuthController@redirectToProvider'));
        }
        if ($provider == "bitbucket") {
            $userId = $user->id;
        } else {
            $userId = $user["id"];
        }

        $authUser = $this->user->findBySocialId($provider, $userId);

        if ($authUser !== null) {
            // User with this social id already exists => login message
            $this->status->addSuccess('auth.login', trans('successes.auth.login'));
        } else {
            $authUser = $this->user->findByEmail($user->email);

            if ($authUser != null) {
                // User with this email already exists => login message
                $this->status->addSuccess('auth.login', trans('successes.auth.login'));
                // Save provider id on it
                $authUser->setProviderId($provider, $userId);
                $authUser->save();
            } else {
                // Create new user with new social id
                $authUser = $this->updater->create([
                    'email' => $user->email,
                    $provider . '_id' => $userId,
                ]);

                $this->status->addSuccess("db.users.registered", trans("successes.db.users.registered"));
            }
        }

        Auth::login($authUser, true);

        return redirect(route('home'));
    }
}
