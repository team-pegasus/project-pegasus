<?php

namespace App\Policies;

use App\Models\Users\User;
use App\Models\Messaging\Thread;
use Illuminate\Auth\Access\HandlesAuthorization;

class ThreadPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can view his own threads
     *
     * @param  User         $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user !== null; // $user->hasPermission('threads.index');
    }

    /**
     * Determine whether the user can view the thead with threads.id = $threadId
     *
     * @param  User         $user
     * @param  Thread       $thread
     * @return mixed
     */
    public function show(User $user, Thread $thread)
    {
        return $user->hasPermission('threads.view') ||
            $thread->users->search(function ($i) {
                return $i->id == $user->id;
            }) !== false;
    }

    /**
     * Determine whether the user can create new threads
     *
     * @param  User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return !empty($user->username) && $user->hasPermission('threads.create');
    }

    /**
     * Determine whether the user can update thread's details
     *
     * @param  User         $user
     * @param  int          $userId
     * @return mixed
     */
    public function update(User $user, $userId)
    {
        return $user->hasPermission('threads.update');
    }

    /**
     * Determine whether the user can delete threads
     *
     * @param  User         $user
     * @return mixed
     */
    public function destroy(User $user)
    {
        return $user->hasPermission('threads.destroy');
    }
}
