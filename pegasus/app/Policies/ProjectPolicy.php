<?php

namespace App\Policies;

use App\Models\Users\User;
use App\Models\Projects\Project;

class ProjectPolicy
{

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can view a list of projects
     *
     * @param  User         $user
     * @return mixed
     */
    public function index(User $user)
    {
        // No idea why this would be false but meh
        return true; // $user->hasPermission('projects.index') || $userId == $user->id;
    }

    /**
     * Determine whether the user can view the a project
     *
     * @param  User         $user
     * @return mixed
     */
    public function show(User $user)
    {
        // No idea why this would be false but meh
        return true; // $user->hasPermission('projects.view') || $userId == $user->id;
    }

    /**
     * Determine whether the user can create a project
     *
     * @param  User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('projects.create');
    }

    /**
     * Determine whether the user can update a project
     *
     * @param  User         $user
     * @param  Project      $project
     * @return mixed
     */
    public function update(User $user, Project $project)
    {
        return $user->hasPermission('projects.update') || $user->id == $project->user_id;
    }

    /**
     * Determine whether the user can delete the userWebsite with user_id $userId
     *
     * @param  User         $user
     * @param  Project      $project
     * @return mixed
     */
    public function destroy(User $user, Project $project)
    {
        return $user->hasPermission('projects.destroy') || $user->id == $project->id;
    }
}
