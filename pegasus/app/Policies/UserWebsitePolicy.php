<?php

namespace App\Policies;

use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserWebsitePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can view the userWebsite with user_id $userId
     *
     * @param  User         $user
     * @param  int          $userId  int of the user_website user_id
     * @return mixed
     */
    public function show(User $user, $userId)
    {
        return $user->hasPermission('users.view') || $userId == $user->id;
    }

    /**
     * Determine whether the user can create userWebsites with user_id $userId
     *
     * @param  User  $user
     * @param  int   $userId  int of the user_website user_id
     * @return mixed
     */
    public function create(User $user, $userId)
    {
        return $user->hasPermission('users.update') || $user->id == $userId;
    }

    /**
     * Determine whether the user can update the userWebsite with user_id $userId
     *
     * @param  User         $user
     * @param  int          $userId
     * @return mixed
     */
    public function update(User $user, $userId)
    {
        return $user->hasPermission('users.update') || $user->id == $userId;
    }

    /**
     * Determine whether the user can delete the userWebsite with user_id $userId
     *
     * @param  User         $user
     * @param  int          $userId
     * @return mixed
     */
    public function destroy(User $user, $userId)
    {
        return $user->hasPermission('users.update') || $user->id == $userId;
    }
}
