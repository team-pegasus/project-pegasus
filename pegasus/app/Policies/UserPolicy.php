<?php

namespace App\Policies;

use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    // use HandlesAuthorization;

    /**
     * Determine whether the user can view the user list.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user_obj
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->hasPermission('users.index');
    }

    /**
     * Determine whether the user can view the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user_obj
     * @return mixed
     */
    public function show(User $user, User $user_obj)
    {
        return $user->hasPermission('users.view');
    }

    /**
     * Determine whether the user can create users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('users.create') || !auth()->check();
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $toUpdate
     * @return mixed
     */
    public function update(User $user, User $toUpdate)
    {
        return $user->hasPermission('users.update') || $user->id == $toUpdate->id;
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $toUpdate
     * @return mixed
     */
    public function destroy(User $user, User $toUpdate)
    {
        return $user->hasPermission('users.delete');
    }
}
