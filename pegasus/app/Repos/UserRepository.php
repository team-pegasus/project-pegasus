<?php

namespace App\Repos;

use App\Models\Users\User;
use App\Repos\Contracts\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function findById($id)
    {
        return $this->user->with('websiteLinks')->find($id);
    }

    public function findByEmail($email)
    {
        return $this->user->byEmail($email)->first();
    }

    public function findByUsername($username)
    {
        return $this->user->byUsername($username)->first();
    }

    public function findBySocialId($provider, $id)
    {
        return $this->user->bySocialId($provider, $id)->first();
    }

    public function getPaginated($perPage = 10, $page = null)
    {
        return $this->user->paginate($perPage, ['*'], $pageName = 'page', $page);
    }

    public function getAll()
    {
        return $this->user->get();
    }
}
