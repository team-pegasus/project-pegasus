<?php

namespace App\Repos;

use App\Models\Users\UserWebsite;
use App\Repos\Contracts\UserWebsiteRepositoryInterface;

class UserWebsiteRepository implements UserWebsiteRepositoryInterface
{

    protected $userWebsite;

    public function __construct(UserWebsite $userWebsite)
    {
        $this->userWebsite = $userWebsite;
    }

    public function findByIds($userId, $websiteId)
    {
        return $this->userWebsite->where([
                'user_id' => $userId,
                'website_id' => $websiteId
            ])->first();
    }
}
