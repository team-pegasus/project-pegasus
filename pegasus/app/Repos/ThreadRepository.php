<?php

namespace App\Repos;

use DB;
use Carbon\Carbon;
use App\Models\Messaging\Thread;
use App\Repos\Contracts\ThreadRepositoryInterface;

class ThreadRepository implements ThreadRepositoryInterface
{

    protected $thread;

    public function __construct(Thread $thread)
    {
        $this->thread = $thread;
    }

    public function findById($id)
    {
        return $this->thread
            ->with(['participants', 'messages.author', 'lastMessage.author'])
            ->find($id);
    }

    public function findPrivateByUserIds($u1id, $u2id)
    {
        return $this->thread
            ->with(['participants', 'messages.author', 'lastMessage.author'])
            ->private()
            ->whereHas('participants', function ($q) use ($u1id, $u2id) {
                $q->whereIn('id', [$u1id, $u2id]);
            })
            ->first();
    }

    public function getByUserId($id)
    {
        return $this->thread->byUserId($id)
            ->with(['participants', 'messages.author', 'lastMessage.author'])->get();
    }
}
