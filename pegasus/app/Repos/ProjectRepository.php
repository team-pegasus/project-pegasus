<?php

namespace App\Repos;

use App\Models\Projects\Project;
use App\Repos\Contracts\ProjectRepositoryInterface;

class ProjectRepository implements ProjectRepositoryInterface
{

    protected $model;

    public function __construct(Project $model)
    {
        $this->model = $model;
    }

    public function findById($id)
    {
        return $this->model->find($id);
    }

    public function getPaginated($perPage = 10, $forPage = null)
    {
        return $this->model->paginate($perPage, ['*'], $pageName = 'page', $forPage);
    }
}
