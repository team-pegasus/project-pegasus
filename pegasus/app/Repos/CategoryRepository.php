<?php

namespace App\Repos;

use App\Models\Categories\Category;
use App\Repos\Contracts\CategoryRepositoryInterface;

class CategoryRepository implements CategoryRepositoryInterface
{

    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function findById($id)
    {
        return $this->category->find($id);
    }

    public function findBySlug($slug)
    {
        return $this->category->bySlug($slug)->first();
    }
}
