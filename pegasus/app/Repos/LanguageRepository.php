<?php

namespace App\Repos;

use App\Models\Categories\Language;
use App\Repos\Contracts\LanguageRepositoryInterface;

class LanguageRepository implements LanguageRepositoryInterface
{

    protected $language;

    public function __construct(Language $language)
    {
        $this->language = $language;
    }

    public function findById($id)
    {
        return $this->language->find($id);
    }

    public function findBySlug($slug)
    {
        return $this->language->bySlug($slug)->first();
    }

    public function getAll()
    {
        return $this->language->get();
    }
}
