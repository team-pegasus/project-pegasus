<?php

namespace App\Repos;

use App\Models\Messaging\Message;
use App\Events\Messaging\NewMessage;
use App\Repos\Contracts\MessageRepositoryInterface;

class MessageRepository implements MessageRepositoryInterface
{

    protected $message;

    public function __construct(Message $message, NewMessage $newMessageEvent)
    {
        $this->message = $message;
        $this->newMessageEvent = $newMessageEvent;
    }

    public function findById($id)
    {
        return $this->message->find($id);
    }

    public function getMessagesAUserCanSeeFromThread($userId, $threadId, $page = 1, $perPage = 20)
    {
        return $this->message->whereHas('thread', function ($q) use ($userId, $threadId) {
            $q->where('threads.id', $threadId)
                ->whereHas('users', function ($q) use ($userId) {
                    $q->where('users.id', $userId)
                        ->whereNull('thread_user.left');
                });
        })->paginate($page, ['*'], $perPage);
    }

    public function getAll()
    {
        return $this->message->get();
    }

    public function create($attributes, $userId, $threadId, $validate = true)
    {
        $message = $this->message->create($attributes, $userId, $threadId, $validate);
        if ($message instanceof Message) {
            // Load author to append to json
            $message->load('author');
            // Fire the NewMessage event
            event(new $this->newMessageEvent($message));
        }

        return $message;
    }
}
