<?php

namespace App\Repos\Contracts;

interface ProjectRepositoryInterface
{

    // $id int
    public function findById($id);

    // $perPage int | items per page
    // $forPage int | for page number
    public function getPaginated($perPage = 10, $forPage = null);
}
