<?php

namespace App\Repos\Contracts;

interface UserWebsiteRepositoryInterface
{

    public function findByIds($userId, $websiteId);
}
