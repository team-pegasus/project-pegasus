<?php

namespace App\Repos\Contracts;

interface ThreadRepositoryInterface
{

    // $id int
    // Returns Thread or null
    public function findById($id);

    // $u1id int first user id
    // $u2id int second user id
    public function findPrivateByUserIds($u1id, $u2id);

    // $userId int
    // Returns collection of all threads
    public function getByUserId($userId);
}
