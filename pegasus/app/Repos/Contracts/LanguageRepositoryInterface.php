<?php

namespace App\Repos\Contracts;

interface LanguageRepositoryInterface
{

    // $id int
    public function findById($id);

    // $slug string
    public function findBySlug($slug);

    public function getAll();
}
