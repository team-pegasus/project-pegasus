<?php

namespace App\Repos\Contracts;

interface MessageRepositoryInterface
{

    // $id int
    public function findById($id);

    // Returns last 20 a user $userId can see from thread $threadId for page $page
    // Default page: 1
    // Per page: 20
    public function getMessagesAUserCanSeeFromThread($userId, $threadId, $page, $perPage);

    public function getAll();
}
