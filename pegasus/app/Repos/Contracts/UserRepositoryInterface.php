<?php

namespace App\Repos\Contracts;

interface UserRepositoryInterface
{

    // $id int
    public function findById($id);

    // $email string
    public function findByEmail($email);

    // $username string
    public function findByUsername($username);

    // $provider 'github'|'facebook'|'bitbucket'
    // $id int
    public function findBySocialId($provider, $id);

    // $perpage int
    // $page int
    public function getPaginated($perPage, $forPage);

    public function getAll();
}
