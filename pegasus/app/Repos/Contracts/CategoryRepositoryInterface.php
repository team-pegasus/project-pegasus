<?php

namespace App\Repos\Contracts;

interface CategoryRepositoryInterface
{

    // $id int
    public function findById($id);

    // $slug string
    public function findBySlug($slug);
}
