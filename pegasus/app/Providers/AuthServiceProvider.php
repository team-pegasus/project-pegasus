<?php

namespace App\Providers;

use Gate;
use App\Policies\UserPolicy;
use Illuminate\Support\Facades\Config;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
    protected $policies = [
        'App\Models\Users\User' => 'App\Policies\UserPolicy',
    ];
     */

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('users.index', 'App\Policies\UserPolicy@index');
        Gate::define('users.create', 'App\Policies\UserPolicy@create');
        Gate::define('users.update', 'App\Policies\UserPolicy@update');

        Gate::define('userwebsites.show', 'App\Policies\UserWebsitePolicy@show');
        Gate::define('userwebsites.create', 'App\Policies\UserWebsitePolicy@create');
        Gate::define('userwebsites.update', 'App\Policies\UserWebsitePolicy@update');
        Gate::define('userwebsites.destroy', 'App\Policies\UserWebsitePolicy@destroy');

        Gate::define('threads.index', 'App\Policies\ThreadPolicy@index');
        Gate::define('threads.show', 'App\Policies\ThreadPolicy@show');
        Gate::define('threads.create', 'App\Policies\ThreadPolicy@create');
        Gate::define('threads.update', 'App\Policies\ThreadPolicy@update');
        Gate::define('threads.destroy', 'App\Policies\ThreadPolicy@destroy');

        Gate::define('projects.index', 'App\Policies\ProjectPolicy@index');
        Gate::define('projects.show', 'App\Policies\ProjectPolicy@show');
        Gate::define('projects.create', 'App\Policies\ProjectPolicy@create');
        Gate::define('projects.update', 'App\Policies\ProjectPolicy@update');
        Gate::define('projects.destroy', 'App\Policies\ProjectPolicy@destroy');
    }
}
