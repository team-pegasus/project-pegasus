<?php

namespace App\Providers;

use DB;
use Validator;
use Illuminate\Support\ServiceProvider;

class ValidationRulesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Value provided mus be empty or unique with
        // table $parameter[0]'s column $parameter[1]
        // Usage: uniqueOrNull:users,username
        Validator::extend('uniqueOrNull', function ($attribute, $value, $parameters, $validator) {
            return empty($value) || (DB::table($parameters[0])->where($parameters[1], '=', $value)->first() === null);
        });

        // Value must be empty or a valid datetime (yyyy-mm-dd hh:mm:ss)
        Validator::extend('dateOrNull', function ($attribute, $value, $parameters, $validator) {
            return empty($value) || (strtotime($value) !== false);
        });

        // Value must be empty or a valid datetime (yyyy-mm-dd hh:mm:ss)
        Validator::extend('notSet', function ($attribute, $value, $parameters, $validator) {
            return !isset($value) || $value === null;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
