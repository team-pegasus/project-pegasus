<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repos\Contracts\UserRepositoryInterface', 'App\Repos\UserRepository');
        $this->app->bind('App\Repos\Contracts\CategoryRepositoryInterface', 'App\Repos\CategoryRepository');
        $this->app->bind('App\Repos\Contracts\ThreadRepositoryInterface', 'App\Repos\ThreadRepository');
        $this->app->bind('App\Repos\Contracts\MessageRepositoryInterface', 'App\Repos\MessageRepository');
        $this->app->bind('App\Repos\Contracts\UserWebsiteRepositoryInterface', 'App\Repos\UserWebsiteRepository');
        $this->app->bind('App\Repos\Contracts\ProjectRepositoryInterface', 'App\Repos\ProjectRepository');

        $this->app->bind('App\Updaters\Contracts\ThreadUpdaterInterface', 'App\Updaters\ThreadUpdater');
        $this->app->bind('App\Updaters\Contracts\MessageUpdaterInterface', 'App\Updaters\MessageUpdater');
        $this->app->bind('App\Updaters\Contracts\UserUpdaterInterface', 'App\Updaters\UserUpdater');
        $this->app->bind('App\Updaters\Contracts\UserWebsiteUpdaterInterface', 'App\Updaters\UserWebsiteUpdater');
        $this->app->bind('App\Updaters\Contracts\ProjectUpdaterInterface', 'App\Updaters\ProjectUpdater');
    }
}
