<?php

namespace App\Listeners\Projects;

use Redis;
use App\Events\Projects\NewProject;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewProjectListener
{

    // Redis channel
    protected $channel;

    // Redis & sockets event name
    protected $eventName;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->channel = env('REDIS_CHANNEL', 'pegasus');
        $this->eventName = 'projects.create';
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $tmp = collect([
            'event' => $this->eventName,
            'data'  => $event->project->toJson()
        ]);
        if (env('APP_ENV') != 'testing') {
            Redis::publish($this->channel, $tmp);
        }
    }
}
