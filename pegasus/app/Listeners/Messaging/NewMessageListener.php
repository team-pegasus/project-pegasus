<?php

namespace App\Listeners\Messaging;

use Redis;
use App\Events\Messaging\NewMessage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewMessageListener
{

    // Redis channel
    protected $channel;

    // Redis & sockets event name
    protected $eventName;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->channel = env('REDIS_CHANNEL', 'pegasus');
        $this->eventName = 'messages.create';
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NewMessage $event)
    {
        $tmp = collect([
            'event' => $this->eventName,
            'data'  => $event->message->toJson()
        ]);
        if (env('APP_ENV') != 'testing') {
            Redis::publish($this->channel, $tmp);
        }
    }
}
