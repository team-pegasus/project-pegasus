<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CustomModel extends Model
{

    // Casts created_at to unix timestamp
    public function getCreatedAtAttribute($timestamp)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $timestamp)->timestamp;
    }

    // Casts updated_at to unix timestamp
    public function getUpdatedAtAttribute($timestamp)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $timestamp)->timestamp;
    }
}
