<?php

namespace App\Models\Projects;

use App\Models\Users\User;
use App\Lib\Traits\Model\Threadable;
use App\Lib\Traits\Model\Languagable;
use App\Models\CustomModel as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{

    // Languages relationship and related methods
    use Languagable;

    // Soft deletes
    use SoftDeletes;

    // Thread relationship (a project can have a thread)
    use Threadable;

    protected $fillable = [
        'name', 'slug', 'description', 'content', 'project_website', 'source_website'
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
    ];

    /**
     * Class Project
     */

    // Scopes
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Accessors

    // Casts deleted_at to unix timestamp
    public function getDeletedAtAttribute($timestamp)
    {
        if ($timestamp === null) {
            return $timestamp;
        }
        return Carbon::createFromFormat('Y-m-d H:i:s', $timestamp)->timestamp;
    }
}
