<?php

namespace App\Models\Users;

use Carbon\Carbon;
use App\Models\Roles\UserRole;
use App\Models\Messaging\Thread;
use App\Models\Users\UserWebsite;
use App\Lib\Traits\Model\Languagable;
use App\Notifications\PasswordResetNotification;
use Illuminate\Support\Facades\Config;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, Languagable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
        'github_id', 'google_id', 'facebook_id',
        'twitter_id', 'bitbucket_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'id' => 'integer',
    ];

    // Relationships
    // Roles
    public function roles()
    {
        return $this->hasMany(UserRole::class);
    }

    // Messaging threads (also private messages)
    public function threads()
    {
        return $this->belongsToMany(Thread::class)
            ->withPivot(['joined', 'left'])
            ->whereNull('thread_user.left')
            ->order();
    }

    // Website links (for github and stuff)
    public function websiteLinks()
    {
        return $this->hasMany(UserWebsite::class)
            ->aUserCanHave()
            ->with('website');
    }

    // Scopes
    public function scopeByEmail($q, $email)
    {
        return $q->where('email', $email);
    }

    public function scopeByUsername($q, $username)
    {
        return $q->where('username', $username);
    }

    public function scopeBySocialId($q, $provider, $id)
    {
        return $q->where($provider . "_id", $id);
    }

    /**
     * Sets provider ID from OAuth
     * @param string $provider github|google|bitbucket
     * @param string $id       ID returned by the provider OAuth
     */
    public function setProviderId($provider, $id)
    {
        $this->{$provider . "_id"} = $id;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }

    /**
     * Check if the user has any of the given roles
     *
     * @param  array of strings  $roles
     * @return bool
     */
    public function hasRoles($roles)
    {
        return !!$this->roles->whereIn('role', $roles)->count();
    }

    /**
     * Check if the user has specific permission
     *
     * @param  string  $permission
     * @return bool
     */
    public function hasPermission($permission)
    {
        $configRoles = config('roles');
        foreach ($this->roles as $role) {
            if (in_array($permission, $configRoles[$role->role])) {
                return true;
            }
            return false; // wtf?
        }
    }

    // Casts created_at to unix timestamp
    public function getCreatedAtAttribute($timestamp)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $timestamp)->timestamp;
    }

    // Casts updated_at to unix timestamp
    public function getUpdatedAtAttribute($timestamp)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $timestamp)->timestamp;
    }

    // Casts updated_at to unix timestamp
    public function getDeletedAtAttribute($timestamp)
    {
        if ($timestamp === null) {
            return $timestamp;
        }
        return Carbon::createFromFormat('Y-m-d H:i:s', $timestamp)->timestamp;
    }
}
