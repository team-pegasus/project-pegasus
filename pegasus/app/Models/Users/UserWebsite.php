<?php

namespace App\Models\Users;

use Carbon\Carbon;
use App\Models\Users\User;
use App\Models\Websites\Website;
use App\Lib\Validators\UserWebsiteValidator;
use App\Models\CustomModel as Model;

class UserWebsite extends Model
{

    protected $fillable = ['url'];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'website_id' => 'integer',
    ];

    // Relationships
    public function website()
    {
        return $this->belongsTo(Website::class)
            ->aUserCanHave();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Scopes
    public function scopeAUserCanHave($q)
    {
        return $q->whereHas('website', function ($q) {
            $q->aUserCanHave();
        });
    }

    public function scopeByIds($q, $userId, $websiteId)
    {
        return $q->where('user_id', $userId)
            ->where('website_id', $websiteId);
    }
}
