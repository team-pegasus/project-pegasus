<?php

namespace App\Models\Websites;

use App\Models\CustomModel as Model;

class Website extends Model
{
    // Scopes
    public function scopeAUserCanHave($q)
    {
        return $q->whereIn('key', ['github', 'custom']);
    }
}
