<?php

namespace App\Models\Messaging;

use Carbon\Carbon;
use App\Models\Users\User;
use App\Models\Messaging\Thread;
use App\Models\CustomModel as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;

    protected $fillable = ['content'];

    protected $with = ['author'];

    protected $casts = [
        'id' => 'integer',
        'thread_id' => 'integer',
        'is_private' => 'boolean',
    ];

    // Relationships
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }

    public function author()
    {
        return $this->owner();
    }

    // Scopes
    public function scopeOrder($q)
    {
        return $q->orderBy('created_at', 'DESC');
    }

    // Accessors

    // Casts deleted_at to unix timestamp
    public function getDeletedAtAttribute($timestamp)
    {
        if ($timestamp === null) {
            return $timestamp;
        }
        return Carbon::createFromFormat('Y-m-d H:i:s', $timestamp)->timestamp;
    }
}
