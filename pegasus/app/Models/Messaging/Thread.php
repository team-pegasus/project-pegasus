<?php

namespace App\Models\Messaging;

use Validator;
use Carbon\Carbon;
use App\Models\Users\User;
use App\Models\Messaging\Message;
use App\Models\CustomModel as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Thread extends Model
{
    use SoftDeletes;

    protected $fillable = ['last_message', 'title'];

    protected $dates = ['last_message'];

    protected $with = ['participants', 'messages.author', 'lastMessage.author'];

    protected $casts = [
        'id' => 'integer',
        'is_private' => 'boolean',
    ];

    // Relationships
    public function users()
    {
        return $this->belongsToMany(User::class)
            ->withPivot(['joined', 'left']);
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function participants()
    {
        return $this->users();
    }

    public function lastMessage()
    {
        return $this->hasOne(Message::class)
            ->orderBy('created_at');
    }

    // Can belong to some other model
    // or it can be null
    public function threadable()
    {
        return $this->morphTo();
    }

    // Scopes
    public function scopeOrder($q)
    {
        return $q->orderBy('last_message', 'DESC');
    }

    // Scopes query to only private threads (1-1)
    public function scopePrivate($q)
    {
        return $q->whereNull('threadable_id');
    }

    /**
     * Threads that a user with id can see. Meaning pivot exists and `left` is null
     * @param  Query    $q      Auto injected
     * @param  int      $userId Parameter
     * @return Query    It's a query scope
     */
    public function scopeByUserId($q, $userId)
    {
        return $q->whereHas('users', function ($q) use ($userId) {
            $q->where('users.id', $userId)
                ->whereNull('thread_user.left');
        });
    }

    // Accessors

    // Casts deleted_at to unix timestamp
    public function getDeletedAtAttribute($timestamp)
    {
        if ($timestamp === null) {
            return $timestamp;
        }
        return Carbon::createFromFormat('Y-m-d H:i:s', $timestamp)->timestamp;
    }

    // Casts last_message to unix timestamp
    public function getLastMessageAttribute($timestamp)
    {
        if ($timestamp === null) {
            return $timestamp;
        }
        return Carbon::createFromFormat('Y-m-d H:i:s', $timestamp)->timestamp;
    }
}
