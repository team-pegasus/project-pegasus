<?php

namespace App\Models\Categories;

use App\Models\CustomModel as Model;
use App\Lib\Traits\Model\Languagable;

class Category extends Model
{

    use Languagable;

    protected $fillable = ['name', 'description'];

    protected $casts = [
        'id' => 'integer',
        'level' => 'integer',
    ];

    /**
     * Class Category
     * Level 0 = highest
     */

    /**
     * Children relationship (many to many)
     * @return Eloquent Query
     */
    public function children()
    {
        return $this->belongsToMany(Category::class, 'category_category', 'parent_id', 'child_id');
    }

    /**
     * Parents relationship (many to many)
     * @return Eloquent Query
     */
    public function parents()
    {
        return $this->belongsToMany(Category::class, 'category_category', 'child_id', 'parent_id');
    }

    /**
     * Parents scoped to 1 level higher
     * @return Eloquent Query
     */
    public function directParents()
    {
        return $this->parents()->byLevel($this->level - 1);
    }

    /**
     * Chidlren scoped to 1 level lower
     * @return Eloquent Query
     */
    public function directChildren()
    {
        return $this->children()->byLevel($this->level + 1);
    }

    public function scopeByLevel($q, $level)
    {
        return $q->where('level', $level);
    }

    public function scopeBySlug($q, $slug)
    {
        return $q->where('slug', $slug);
    }

    public function scopeByName($q, $name)
    {
        return $q->where('name', 'LIKE', '%' . $name . '%');
    }
}
