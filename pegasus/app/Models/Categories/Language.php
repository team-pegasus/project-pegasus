<?php

namespace App\Models\Categories;

use Validator;
use App\Models\CustomModel as Model;

// Programming languages (go, js, php...)
class Language extends Model
{

    protected $fillable = ['name', 'short', 'description'];

    protected $casts = [
        'id' => 'integer',
    ];

    // Scopes
    public function scopeBySlug($q, $slug)
    {
        return $q->where('slug', $slug);
    }

    public function scopeByName($q, $name)
    {
        return $q->where('name', $name);
    }
}
