<?php

namespace App\Models\Roles;

use App\Models\Users\User;
use App\Lib\Roles\PermissionFactory;
use App\Models\CustomModel as Model;

class UserRole extends Model
{
    protected $permissions = null;

    protected $table = 'users_roles';

    protected $fillable = [
        'role', 'user_id'
    ];
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Universal wrapper for getting role name
    // since config depends on it
    public function getName()
    {
        return $this->role;
    }

    // Returns a collection of role's permissions
    public function getPermissions()
    {
        // fetch them
        if ($this->permissions === null) {
            $this->permissions = PermissionFactory::makeManyGeneral(config('roles.' . $this->getName()));
        }
        return $this->permissions;
    }

    // Searches permissions for the $permission
    public function hasPermission($permission)
    {
        return $this->getPermissions()->search(function ($i, $k) use ($permission) {
            return $i == $permission;
        }) !== false;
    }
}
