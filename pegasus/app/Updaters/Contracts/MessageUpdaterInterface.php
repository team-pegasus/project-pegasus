<?php

namespace App\Updaters\Contracts;

interface MessageUpdaterInterface
{

    /**
     * Creates a new message
     * @param  array $attributes Array of attributes (content only 27/08/17)
     * @param  int   $userId     User id that created the message
     * @param  int   $threadId   Thread id that the message belongs to
     * @return Message|ErrorBag  Message model on success, ErrorBag on fail
     */
    public function create($attributes, $userId, $threadId, $validate = true);
}
