<?php

namespace App\Updaters\Contracts;

interface UserUpdaterInterface
{

    // $id int
    // $attributes array
    // $validate bool
    public function updateById($id, $attributes, $validate);

    // $attributes Array of attributes
    // $validate bool
    public function create($attributes, $validate);
}
