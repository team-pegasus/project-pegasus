<?php

namespace App\Updaters\Contracts;

interface UserWebsiteUpdaterInterface
{

    // $userId int
    // $websiteId int
    // $url string
    // $validate bool
    public function updateByIds($userId, $websiteId, $url);

    // $attributes Array of attributes
    // $validate bool
    public function create($attributes, $validate);

    // $userId int
    // $websiteId int
    public function deleteByIds($userId, $websiteId);
}
