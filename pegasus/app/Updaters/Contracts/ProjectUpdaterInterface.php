<?php

namespace App\Updaters\Contracts;

interface ProjectUpdaterInterface
{

    // $attributes array | thread attributes
    // $userIds id | owner id
    // $validate bool | should the attributes be validated
    public function create($attributes, $userId, $validate = true);
}
