<?php

namespace App\Updaters\Contracts;

interface ThreadUpdaterInterface
{

    // $attributes array | thread attributes
    // $userIds array | users in thread
    // $morphTo Model | Thread morphs to what object. Private thread if null
    // $validate bool | should the attributes be validated
    public function create($attributes, $userIds = [], $morphTo = null, $validate = true);

    // Add user with $userId to thread with $threadId
    // $threadId int | Thread's id
    // $userId int | User's id
    public function addUserToThread($threadId, $userId);

    // Remove user with $userId from thread with $threadId
    // $threadId int | Thread's id
    // $userId int | User's id
    public function removeUserFromThread($threadId, $userId);
}
