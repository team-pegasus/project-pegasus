<?php

namespace App\Updaters;

use DB;
use Carbon\Carbon;
use App\Models\Users\UserWebsite;
use App\Lib\Validators\UserWebsiteValidator;
use App\Updaters\Contracts\UserWebsiteUpdaterInterface;

class UserWebsiteUpdater implements UserWebsiteUpdaterInterface
{

    public function __construct(UserWebsite $model, UserWebsiteValidator $validator)
    {
        $this->validator = $validator;
        $this->model = $model;
    }

    /**
     * Creates UserWebsite from key => val array [$columnName => $value]
     * @param  array   $attributes Array of attributes
     * @param  boolean $validate   Should the model be validated before saving
     * @return User|ErrorBag       User model on success, ErrorBag on fail
     */
    public function create($attributes, $validate = true)
    {
        if ($validate === true &&
            ($errors = $this->validator->create($attributes)) !== true) {
            return $errors;
        }

        $uw = $this->model;
        $uw->user_id = $attributes['user_id'];
        $uw->website_id = $attributes['website_id'];
        $uw->url = $attributes['url'];
        $uw->save();

        return $uw;
    }

    /**
     * Updates UserWebsite url
     * @param  int          $userId     user_id of record to update
     * @param  int          $websiteId  website_id of record to update
     * @param  string       $url        The new url
     * @return int|ErrorBag             1 on success, ErrorBag on fail
     */
    public function updateByIds($userId, $websiteId, $url)
    {
        $attributes['updated_at'] = Carbon::now();
        $attributes['url'] = $url;
        return $this->model->byIds($userId, $websiteId)->update($attributes);
    }
    /**
     * Deletes UserWebsite
     * @param  int          $userId     user_id of record to update
     * @param  int          $websiteId  website_id of record to update
     * @return int
    */
    public function deleteByIds($userId, $websiteId)
    {
        return $this->model->byIds($userId, $websiteId)->delete();
    }
}
