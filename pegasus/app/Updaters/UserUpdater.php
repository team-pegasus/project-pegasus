<?php

namespace App\Updaters;

use DB;
use Carbon\Carbon;
use App\Models\Users\User;
use App\Lib\Validators\UserValidator;
use App\Updaters\Contracts\UserUpdaterInterface;

class UserUpdater implements UserUpdaterInterface
{

    public function __construct(User $model, UserValidator $validator)
    {
        $this->model = $model;
        $this->validator = $validator;
    }

    /**
     * Creates user from key => val array [$columnName => $value]
     * WARNING: expects hashed password
     * @param  array   $attributes Array of attributes
     * @param  boolean $validate   Should the model be validated before saving
     * @return User|ErrorBag     User model on success, ErrorBag on fail
     */
    public function create($attributes, $validate = true)
    {
        if ($validate === true && ($errors = $this->validator->create($attributes)) !== true) {
            return $errors;
        }

        $user = $this->model;
        $user->fill($attributes);
        $user->save();

        // Temporary solution
        DB::table('users_roles')->insert([
            'role' => config('app.default_user_role'),
            'user_id' => $user->id
        ]);

        return $user;
    }

    /**
     * Updates user's attributes
     * @param  imt  $userId         id of user to update
     * @param  array  $attributes   attributes to update
     * @param  boolean $validate    Should the attributes be validated
     * @return int|ErrorBag         1 on success, ErrorBag on fail
     */
    public function updateById($userId, $attributes, $validate = true)
    {
        $user = $this->model->findOrFail($userId);
        if ($validate === true
            && ($errors = $this->validator->update($user->getAttributes(), $attributes)) !== true) {
            return $errors;
        }
        $attributes['updated_at'] = Carbon::now();
        return $this->model->where('id', $userId)->update($attributes);
    }
}
