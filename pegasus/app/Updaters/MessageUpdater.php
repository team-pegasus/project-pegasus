<?php

namespace App\Updaters;

use DB;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use App\Models\Messaging\Message;
use App\Events\Messaging\NewMessage;
use App\Lib\Validators\MessageValidator;
use App\Updaters\Contracts\MessageUpdaterInterface;

class MessageUpdater implements MessageUpdaterInterface
{

    // Message model
    protected $model;

    // Database validator for messages
    protected $validator;

    // New message event
    protected $newMessageEvent;

    public function __construct(Message $model, MessageValidator $validator, NewMessage $newMessageEvent)
    {
        $this->model = $model;
        $this->validator = $validator;
        $this->newMessageEvent = $newMessageEvent;
    }

    /**
     * Creates a new message
     * @param  array $attributes Array of attributes (content only 27/08/17)
     * @param  int   $userId     User id that created the message
     * @param  int   $threadId   Thread id that the message belongs to
     * @return Message|ErrorBag  Message model on success, ErrorBag on fail
     */
    public function create($attributes, $userId, $threadId, $validate = true)
    {
        if ($validate === true && ($errors = $this->validator->create($attributes)) !== true) {
            return $errors;
        }

        $message = $this->model;
        $message->user_id = $userId;
        $message->thread_id = $threadId;
        $message->content = Arr::get($attributes, 'content');
        $message->save();

        $message->load(['author']);

        // Fire the NewMessage event
        event(new $this->newMessageEvent($message));

        return $message;
    }
}
