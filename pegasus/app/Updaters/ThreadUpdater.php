<?php

namespace App\Updaters;

use DB;
use Validator;
use Carbon\Carbon;
use App\Models\Messaging\Thread;
use Illuminate\Database\Eloquent\Model;
use App\Lib\Validators\ThreadValidator;
use App\Updaters\Contracts\ThreadUpdaterInterface;

class ThreadUpdater implements ThreadUpdaterInterface
{

    public function __construct(Thread $model, ThreadValidator $validator)
    {
        $this->model = $model;
        $this->validator = $validator;
    }

    public function create($attributes, $userIds = [], $morphTo = null, $validate = true)
    {
        if ($validate === true && ($errors = $this->validator->create($attributes, $userIds, $morphTo)) !== true) {
            return $errors;
        }

        $model = $this->model;
        $model->fill($attributes);
        if ($morphTo instanceof Model) {
            $model->threadable()->associate($morphTo);
        }
        $model->save();

        $model->users()->sync($userIds);

        $model->load(['participants', 'messages.author', 'lastMessage.author']);

        return $model;
    }

    public function addUserToThread($threadId, $userId)
    {
        if (($errors = $this->validator->addUserToThread($threadId, $userId)) !== true) {
            return $errors;
        }
        // Check if user already is/was in thread
        $existing = DB::table('thread_user')
            ->where('thread_id', $threadId)->where('user_id', $userId)->first();
        // If not create a new pivot
        if ($existing === null) {
            return DB::table('thread_user')->insert([
                    'thread_id' => $threadId,
                    'user_id' => $userId,
                    'joined' => Carbon::now(),
                    'left' => null
                ]);
        }
        // If it already is in thread, do nothing
        if ($existing->left === null) {
            return null;
        }
        // Otherwise (user rejoins) set `left` to null and `joined` to now
        return DB::table('thread_user')
            ->where('thread_id', $threadId)->where('user_id', $userId)
            ->update([
                    'left' => null,
                    'joined' => Carbon::now()
                ]);
    }

    public function removeUserFromThread($threadId, $userId)
    {
        return DB::table('thread_user')->where([
                'thread_id' => $threadId,
                'user_id' => $userId
            ])->update(['left' => Carbon::now()]);
    }
}
