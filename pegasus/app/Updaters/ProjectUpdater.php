<?php

namespace App\Updaters;

use Carbon\Carbon;
use App\Models\Projects\Project;
use App\Events\Projects\NewProject;
use App\Lib\Validators\ProjectValidator;
use App\Updaters\Contracts\ProjectUpdaterInterface;

class ProjectUpdater implements ProjectUpdaterInterface
{

    // Project model
    protected $model;

    // Database validator for projects
    protected $validator;

    // New project event
    protected $newProjectEvent;

    public function __construct(Project $model, ProjectValidator $validator, NewProject $newProjectEvent)
    {
        $this->model = $model;
        $this->validator = $validator;
        $this->newProjectEvent = $newProjectEvent;
    }

    /**
     * Creates aa new project
     * @param  array    $attributes Project's attributes
     * @param  int      $userId     Author's id
     * @param  boolean  $validate   Validate?
     * @return Project|MessageBag   Project model on success, Error bag on fail
     */
    public function create($attributes, $userId, $validate = true)
    {
        if ($validate === true && ($errors = $this->validator->create($attributes)) !== true) {
            return $errors;
        }

        $model = $this->model;
        $model->user_id = $userId;
        $model->fill($attributes);
        $model->slug = str_slug($model->name);
        $model->save();

        // Fire the NewMessage event
        event(new $this->newProjectEvent($model));

        return $model;
    }

    /**
     * Updates a project by it's id
     * @param  int      $projectId  id of project to update
     * @param  array    $attributes project's attributes to update
     * @param  boolean  $validate   Validate?
     * @return int|ErrorBag         1 on success, ErrorBag on fail
     */
    public function updateById(Project $project, array $attributes, $validate = true)
    {
        if ($validate === true && ($errors = $this->validator->update($attributes)) !== true) {
            return $errors;
        }

        if (isset($attributes['name'])) {
            $attributes['slug'] = str_slug($attributes['name']);
        }
        $project->fill($attributes);
        $project->save();

        return $project;
    }
}
