<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Redis;

Route::get('sockets', function () {
    $m = new \App\Models\Messaging\Message;
    $m->user_id = 1;
    event(new \App\Events\Messaging\NewMessage($m));
    return 'Event sent';
})->name('test');

Route::get('sockets-test', function () {
    return view('sockets');
})->name('test');

// Social OAuth routes
Route::get('auth/{provider}', 'Auth\OAuthController@redirectToProvider')
    ->name('oauth')
    ->where(['provider' => '(github|google|bitbucket)']);

Route::get('auth/{provider}/callback', 'Auth\OAuthController@handleProviderCallback')
    ->where(['provider' => '(github|google|bitbucket)']);

// Api routes
Route::group(['as' => 'api.', 'prefix' => 'api'], function () {

    Route::resource('users', 'UserController');

    Route::group(['as' => 'users.websites', 'prefix' => 'users/websites'], function () {
        Route::post('/', 'UserWebsiteController@create');
        Route::get('/{userId}/{websiteId}', 'UserWebsiteController@show');
        Route::put('/{userId}/{websiteId}', 'UserWebsiteController@update');
        Route::patch('/{userId}/{websiteId}', 'UserWebsiteController@update');
        Route::delete('/{userId}/{websiteId}', 'UserWebsiteController@destroy');
    });
    // Users
    Route::group(['as' => 'users.', 'prefix' => 'users'], function () {

        // Authentication routes
        Route::group(['as' => 'auth.', 'prefix' => 'current'], function () {

            Route::get('current', 'Auth\UserController@getCurrent')
                ->middleware(['auth'])
                ->name('current');

            // Authentication Routes...
            Route::get('login', 'Auth\LoginController@showLoginForm')
                ->name('getLogin');
            Route::post('login', 'Auth\LoginController@login')
                ->name('postLogin');
            Route::post('auth/login', 'Auth\LoginController@login')
                ->name('authpostLogin');

            // Registration Routes...
            Route::get('register', 'Auth\RegisterController@showRegistrationForm')
                ->name('getRegister');
            Route::post('register', 'Auth\RegisterController@register')
                ->name('postRegister');

            // Password Reset Routes...
            Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')
                ->name("getPasswordReset");
            Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')
                ->name("postPasswordResetEmail");
        });
    });

    // Threads
    Route::group(['as' => 'threads.', 'prefix' => 'threads'], function () {
        Route::post('/', 'ThreadController@store');
        Route::get('/', 'ThreadController@index');
        Route::post('{threadId}/message', 'MessageController@create');
    });

    // Messages
    Route::group(['as' => 'messages.', 'prefix' => 'messages'], function () {
        Route::post('/new', 'MessageController@new');
        Route::get('/', 'MessageController@index');
        Route::post('{threadId}/message', 'MessageController@create');
    });

    // Projects
    Route::group(['as' => 'projects.', 'prefix' => 'projects'], function () {
        Route::post('/', 'ProjectController@create')->name('create');
        Route::get('/', 'ProjectController@index')->name('index');
        Route::get('{id}', 'ProjectController@show')->name('show');
        Route::put('{id}', 'ProjectController@update')->name('update');
        Route::patch('{id}', 'ProjectController@update')->name('update');
    });
});

Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')
    ->name("auth.getPasswordResetForm");
Route::post('password/reset', 'Auth\ResetPasswordController@reset')
    ->name("auth.postPasswordReset");

Route::get('logout', 'Auth\LoginController@logout')
    ->name('logout');

Route::get('/', function () {
    return view('layouts.spalayout');
})->name('home');

Route::get('{any}', function () {
    return view('layouts.spalayout');
})
->where('any', '(.*)')
->name('spa');
