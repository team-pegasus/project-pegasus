<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User roles
    |--------------------------------------------------------------------------
    |
    | This file defines all user roles and their respective permissions.
    | Admin has all permissions available and as such all permissions
    | are described in it's list.
    |
    */

    'admin' => [
        /* ======== USER PERMISSIONS ======== */
        // View a list of all users
        'users.index',
        // View user details
        'users.view',
        // Create new users
        'users.create',
        // Edit users
        'users.update',
        // Delete users
        'users.delete',


        // View a list of all users
        'threads.index',
        // View user details
        'threads.view',
        // Create new threads
        'threads.create',
        // Edit threads
        'threads.update',
        // Delete threads
        'threads.destroy',


        // View a list of all projects
        'projects.index',
        // View project details
        'projects.view',
        // Create new projects
        'projects.create',
        // Edit projects
        'projects.update',
        // Delete projects
        'projects.destroy',
    ],
    'editor' => [
        'users.index',
    ],
    'user' => [
        'users.index',
    ],
];
