<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Thread role permissions
    |--------------------------------------------------------------------------
    |
    | Each thread_user pivot has a string 'role'.
    | Here are all permissions and which roles have the permissions.
    |
    */

    /**
     * Permissions:
     * messages.create => User can post messages to the thread
     * users.add => User can add users to the thread
     * users.remove => User ban users from the thread
     */

    'admin' => [
        /* ======== USER PERMISSIONS ======== */
        'messages.create',
        'users.add',
        'users.remove',
    ],
    'guest' => [
        /* ======== USER PERMISSIONS ======== */
        'messages.create',
    ],
    'banned' => []
];
