# PROJECT PEGASUS

## Testing
php artisan migrate --database=testing
./vendor/bin/phpunit ./tests

## Install

### Ubuntu
sudo apt-get install php-common php-mbstring php-xml php7.0-mysql
composer install

## make new feature
git pull --all
git checkout development
git checkout -b feature/new-feature-branch
git push -u origin feature/new-feature-branch

## Run for development
1. Run php side with `php artisan serve` or Homeastead
2. Run frontend with `npm run watch` for develop
3. Run sockets.js with `node sockets.js` for sockets

## Technologies used and required
* PHP 7+
* Node for sockets and JS compiling
* Redis for event broadcasting from PHP to sockets

