<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('websites')) {
            Schema::create('websites', function (Blueprint $table) {
                $table->increments('id');
                $table->string('key');
                $table->string('name');
                $table->string('url');
                $table->string('thumbnail_url')->nullable();
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('user_websites')) {
            Schema::create('user_websites', function (Blueprint $table) {
                $table->primary(['user_id', 'website_id']);
                $table->integer('user_id')->unsigned();
                $table->integer('website_id')->unsigned();
                $table->string('url');
                $table->timestamps();

                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

                $table->foreign('website_id')
                    ->references('id')
                    ->on('websites')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_websites');
        Schema::dropIfExists('websites');
    }
}
