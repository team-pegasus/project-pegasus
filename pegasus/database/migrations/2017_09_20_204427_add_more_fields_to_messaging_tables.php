<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreFieldsToMessagingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('threads', 'private_message')) {
            Schema::table('threads', function (Blueprint $table) {
                $table->boolean('is_private')->default(0)->after('id');
            });
        }
        if (!Schema::hasColumn('thread_user', 'role')) {
            Schema::table('thread_user', function (Blueprint $table) {
                $table->string('role')->nullable('after')->after('user_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
