<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('threads')) {
            Schema::create('threads', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title')->nullable();
                $table->timestamp('last_message')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('thread_user')) {
            Schema::create('thread_user', function (Blueprint $table) {
                $table->integer('thread_id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->timestamp('joined')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('left')->nullable();

                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

                $table->foreign('thread_id')
                    ->references('id')
                    ->on('threads')
                    ->onDelete('cascade');

                $table->primary(['thread_id', 'user_id']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thread_user');
        Schema::dropIfExists('threads');
    }
}
