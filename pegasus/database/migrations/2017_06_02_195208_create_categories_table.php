<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('categories')) {
            Schema::create('categories', function (Blueprint $table) {
                $table->increments('id');
                $table->smallInteger('level')->unsigned();
                $table->string('name');
                $table->string('slug');
                $table->text('description')->nullable();
                $table->timestamps();
            });

            Schema::create('category_category', function (Blueprint $table) {
                $table->integer('parent_id')->unsigned();
                $table->integer('child_id')->unsigned();
                $table->primary(['parent_id', 'child_id']);

                $table->foreign('parent_id')
                    ->references('id')
                    ->on('categories')
                    ->onDelete('cascade');

                $table->foreign('child_id')
                    ->references('id')
                    ->on('categories')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_category');
        Schema::dropIfExists('categories');
    }
}
