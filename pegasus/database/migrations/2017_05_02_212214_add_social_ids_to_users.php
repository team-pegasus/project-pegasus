<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSocialIdsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('users', 'github_id')) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('github_id')
                    ->nullable()
                    ->default(null)
                    ->after('id');
            });
        }

        if (!Schema::hasColumn('users', 'google_id')) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('google_id')
                    ->nullable()
                    ->default(null)
                    ->after('github_id');
            });
        }

        if (!Schema::hasColumn('users', 'facebook_id')) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('facebook_id')
                    ->nullable()
                    ->default(null)
                    ->after('google_id');
            });
        }

        if (!Schema::hasColumn('users', 'twitter_id')) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('twitter_id')
                    ->nullable()
                    ->default(null)
                    ->after('facebook_id');
            });
        }

        if (!Schema::hasColumn('users', 'bitbucket_id')) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('bitbucket_id')
                    ->nullable()
                    ->default(null)
                    ->after('twitter_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
