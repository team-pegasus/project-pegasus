<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Users\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->unique()->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Models\Categories\Category::class, function (Faker\Generator $faker) {
    return [
        'level' => $faker->numberBetween(0, 5),
        'name' => $faker->name,
        'slug' => $faker->slug,
        'description' => collect($faker->sentences(4))->implode('. '),
    ];
});


$factory->define(App\Models\Categories\Language::class, function (Faker\Generator $faker) {
    return [
        'short' => str_random(8),
        'name' => $faker->name,
        'slug' => $faker->slug,
        'description' => collect($faker->sentences(4))->implode('. '),
    ];
});

$factory->define(App\Models\Messaging\Thread::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->name,
    ];
});

$factory->define(App\Models\Messaging\Message::class, function (Faker\Generator $faker) {
    return [
        'user_id' => factory(App\Models\Users\User::class)->create()->id,
        'thread_id' => factory(App\Models\Messaging\Thread::class)->create()->id,
        'content' => "Some really long message"
    ];
});

$factory->define(App\Models\Websites\Website::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->domainName,
        'key' => $faker->domainWord,
        'url' => $faker->url
    ];
});

$factory->define(App\Models\Users\UserWebsite::class, function (Faker\Generator $faker) {
    return [
        'user_id' => factory(App\Models\Users\User::class)->create()->id,
        'website_id' => factory(App\Models\Websites\Website::class)->create()->id,
        'url' => $faker->url
    ];
});

$factory->define(App\Models\Projects\Project::class, function (Faker\Generator $faker) {
    return [
        'user_id' => factory(App\Models\Users\User::class)->create()->id,
        'name' => $faker->domainWord,
        'slug' => str_slug($faker->domainWord),
        'description' => collect($faker->sentences(4))->implode('. '),
        'content' => collect($faker->paragraphs(5))->implode('. <br>'),
        'project_website' => $faker->url,
        'source_website' => $faker->url,
    ];
});
