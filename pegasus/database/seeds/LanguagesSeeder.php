<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Models\Categories\Language;

class LanguagesSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        $languages = [
            [
                'short' => 'php',
                'name' => 'php',
                'description' => 'PHP is a server scripting language, and a powerful tool for making dynamic and interactive Web pages.',
            ],
            [
                'short' => 'python',
                'name' => 'python',
                'description' => 'Python is a widely used high-level programming language for general-purpose programming, created by Guido van Rossum and first released in 1991.',
            ],
            [
                'short' => 'Ruby',
                'name' => 'Ruby',
                'description' => 'Ruby is a dynamic, reflective, object-oriented, general-purpose programming language.',
            ],
            [
                'short' => 'C#',
                'name' => 'C#',
                'description' => 'C# is a multi-paradigm programming language encompassing strong typing, imperative, declarative, functional, generic, object-oriented, and component-oriented programming disciplines.',
            ],
            [
                'short' => 'Node js',
                'name' => 'Node js',
                'description' => 'Node.js is a JavaScript runtime built on Chrome\'s V8 JavaScript.',
            ],
            [
                'short' => 'Java',
                'name' => 'Java',
                'description' => 'Java is a general-purpose computer programming language that is concurrent, class-based, object-oriented, and specifically designed to have as few implementation dependencies as possible.',
            ],
            [
                'short' => 'Go',
                'name' => 'Golang',
                'description' => 'Go is a general-purpose language designed with systems programming in mind.',
            ],
        ];

        if (env('env') !== 'testing') {
            foreach ($languages as $l) {
                if (Language::where('name', $l['name'])->first() === null) {
                    $l['slug'] = str_slug($l['name']);
                    factory(Language::class)->create($l);
                    $this->command->info("Inserted " . $l['name']);
                }
            }
        }
    }
}
