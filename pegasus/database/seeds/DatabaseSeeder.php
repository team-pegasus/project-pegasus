<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(WebsitesSeeder::class); // Seeds github/custom website
        $this->call(LanguagesSeeder::class); // Seeds programming languages
        $this->call(CategoriesSeeder::class); // Seeds programming languages
    }
}
