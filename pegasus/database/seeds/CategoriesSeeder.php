<?php

use Illuminate\Database\Seeder;
use App\Models\Categories\Language;
use App\Models\Categories\Category;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();

        $cats = [
            [
                'level' => 1,
                'name' => 'Django',
                'description' => 'Django is a high-level Python Web framework that encourages rapid development and clean, pragmatic design.',
                'lang' => 'python',
            ],
            [
                'level' => 1,
                'name' => 'Elixir',
                'description' => 'It is a fairly thin wrapper, which provides the ability to create simple Python classes that map directly to relational database tables, providing many of the benefits of traditional databases without losing the convenience of Python objects.',
                'lang' => 'python',
            ],
            [
                'level' => 1,
                'name' => 'Laravel',
                'description' => 'A PHP Framework For Web Artisans.',
                'lang' => 'php',
            ],
            [
                'level' => 1,
                'name' => 'Express',
                'description' => 'Fast, unopinionated, minimalist web framework for Node.js.',
                'lang' => 'Node js',
            ],
            [
                'level' => 1,
                'name' => 'Ruby on Rails',
                'description' => 'Rails is a model–view–controller framework, providing default structures for a database, a web service, and web pages.',
                'lang' => 'Ruby',
            ],
        ];

        foreach ($cats as $cat) {
            if (Category::where('name', $cat['name'])->first() == null) {
                $lang = Language::where('name', $cat['lang'])->first();

                $tmp = new Category;
                $tmp->slug = str_slug($cat['name']);
                $tmp->name = $cat['name'];
                $tmp->level = $cat['level'];
                $tmp->description = $cat['description'];
                $tmp->save();

                $tmp->languages()->attach($lang->id);

                $this->command->info("Inserted " . $cat['name']);
            }
        }
    }
}
