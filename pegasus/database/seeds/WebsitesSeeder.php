<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Models\Websites\Website;

class WebsitesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        $websites = [
            [
                'key' => 'github',
                'name' => 'Github',
                'url' => 'https://github.com/'
            ],
            [
                'key' => 'custom',
                'name' => 'Main',
                'url' => ''
            ],
        ];

        if (env('env') !== 'testing') {
            foreach ($websites as $w) {
                if (Website::where('key', $w['key'])->first() === null) {
                    factory(Website::class)->create($w);
                    $this->command->info("Inserted " . $w['key']);
                }
            }
        }
    }
}
