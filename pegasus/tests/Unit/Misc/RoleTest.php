<?php

namespace Tests\Unit\Misc;

use Tests\TestCase;
use App\Lib\Roles\Role;

class RoleTest extends TestCase
{
    public function __construct()
    {
        parent::__construct();
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testHasPermission()
    {
        $role = new Role('a', ['test', 'b']);
        $this->assertTrue($role->hasPermission('test'));
    }

    // Tests whether permissions become a collection as they're supposed to
    public function testGetPermissions()
    {
        $permissions = ['a', 'b'];
        $role = new Role('a', $permissions);
        $this->assertEquals($role->getPermissions(), collect($permissions));
    }
}
