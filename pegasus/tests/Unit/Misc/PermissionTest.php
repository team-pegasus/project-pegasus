<?php

namespace Tests\Unit\Misc;

use Tests\TestCase;
use App\Lib\Roles\Permission;

class PermissionTest extends TestCase
{
    public function __construct()
    {
        parent::__construct();
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testBelongsToRole()
    {
        $permission = new Permission('a', ['test', 'b']);
        $this->assertTrue($permission->belongsToRole('test'));
    }

    // Tests whether roles become a collection as they're supposed to
    public function testGetRoles()
    {
        $roles = ['a', 'b'];
        $p = new Permission('a', $roles);
        $this->assertEquals($p->getRoles(), collect($roles));
    }
}
