<?php

namespace Tests\Integration\Validators;

use Tests\TestCase;
use App\Models\Users\User;
use App\Updaters\ThreadUpdater;
use App\Models\Messaging\Thread;
use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ThreadUpdaterTest extends TestCase
{

    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->model = app(Thread::class);
        $this->updater = app(ThreadUpdater::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testCreateSuccess()
    {
        // Cool timestamp
        $attributes = [
            'last_message' => '2017-09-09 00:00:00'
        ];
        // Create random user
        $user = factory(User::class)->create(['id' => 98580]);
        $user2 = factory(User::class)->create();
        // Create a thread with cool ts
        $result = $this->updater->create($attributes, [$user->id, $user2->id]);
        // Find the created thread with Thread model (not repo)
        $expected = $this->model->where($attributes)->first();
        // Assert the $expected thread is same as $result from repo->create()
        $this->assertEquals($expected->getKey(), $result->getKey());
        // Assert the $result thread belongs to the $user user
        $this->assertEquals($user->getKey(), $result->users()->first()->getKey());

        // Assert that 3rd argument (morphTo) works
        $morphTo = $user;
        $r = $this->updater->create([], [$user->id], $morphTo);
        $r->load('threadable');
        $this->assertNotNull($r->threadable);
    }

    public function testCreateWrongDateFormatFail()
    {
        // Not cool timestamp
        $attributes = [
            'last_message' => '2017-13-09 00:00:00'
        ];
        // Create random user
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        // Create with not cool ts
        $result = $this->updater->create($attributes, [$user->id, $user2->id]);
        // Result should be ErrorBag and should have error `last_message`
        $this->assertTrue($result->has('last_message'));
    }

    public function testCreatePrivateThreadAlreadyExistsFail()
    {
        $u1 = factory(User::class)->create();
        $u2 = factory(User::class)->create();
        // Create 1 successfully
        $thread = $this->updater->create([], [$u1->id, $u2->id]);
        // Create another 1 unsuccessfully
        $r = $this->updater->create([], [$u2->id, $u1->id]);
        $this->assertTrue($r instanceof MessageBag);
    }
}
