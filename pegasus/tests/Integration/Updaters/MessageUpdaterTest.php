<?php

namespace Tests\Integration\Validators;

use Tests\TestCase;
use App\Models\Users\User;
use App\Updaters\MessageUpdater;
use App\Models\Messaging\Thread;
use App\Models\Messaging\Message;
use Illuminate\Support\MessageBag;
use App\Events\Messaging\NewMessage;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MessageUpdaterTest extends TestCase
{

    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->updater = app(MessageUpdater::class);
        $this->model = app(Message::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testCreateSuccess()
    {
        $user = factory(User::class)->create();
        $thread = factory(Thread::class)->create();
        $attributes = [
            'content' => 'bla bla bla'
        ];
        $this->expectsEvents(NewMessage::class);
        // Create message to $user and $thread
        $result = $this->updater->create($attributes, $user->id, $thread->id);
        // Find the messaage without dependencies (through model)
        $expected = $this->model->where($attributes)->first();
        // Assert $expected id = $result ids
        $this->assertEquals($expected->getKey(), $result->getKey());
        // Assert $user is the owner of $result message
        $this->assertEquals($user->getKey(), $result->owner->getKey());
        // Assert $result message belongs to $thread
        $this->assertEquals($thread->getKey(), $result->thread->getKey());
    }

    public function testCreateNoContentFail()
    {
        $user = factory(User::class)->create();
        $thread = factory(Thread::class)->create();

        $attributes = [];
        // Create a message without content
        $result = $this->updater->create($attributes, $user->id, $thread->id);
        // Result should be a messageBag
        $this->assertTrue($result instanceof MessageBag);
        // Result should be ErrorBag and should have error `content`
        $this->assertTrue($result->has('content'));
    }
}
