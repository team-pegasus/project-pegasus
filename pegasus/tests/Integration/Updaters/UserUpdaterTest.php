<?php

namespace Tests\Integration\Validators;

use Tests\TestCase;
use App\Models\Users\User;
use App\Updaters\UserUpdater;
use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserUpdaterTest extends TestCase
{

    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->updater = app(UserUpdater::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testUpdateSuccess()
    {
        $u = factory(User::class)->create(['username' => null, 'github_id' => 'sth']);
        $r = $this->updater->updateById($u->id, ['username' => 'sth']);
        $this->assertEquals(1, $r);
        $u->refresh();
        $this->assertEquals('sth', $u->username);
    }

    public function testUpdateFailUsername()
    {
        $u = factory(User::class)->create(['username' => 'abc']);
        // Check that it fails if you try to update username
        $r = $this->updater->updateById($u->id, ['username' => 'test']);
        // Check that result ErrorBag has `username`
        $this->assertTrue($r->has('username'));
    }

    public function testCreateSuccess()
    {
        $u = $this->updater->create([
            'username' => 'lalala',
            'email' => 'lala@lala.com',
            'password' => \Hash::make('lala')
        ]);
        $this->assertTrue($u->created_at != null);
        foreach (['github', 'google', 'facebook', 'twitter', 'bitbucket'] as $p) {
            $u = $this->updater->create([
                'email' => $p . '@lala.com',
                'password' => \Hash::make('lala'),
                $p . '_id' => 123
            ]);
            // Assert that the user was really created
            $this->assertTrue($u->exists());
            // Assert that the user received a default role
            $this->assertTrue($u->hasRoles([config('app.default_user_role')]));
            $this->assertEquals(123, $u->{$p . '_id'});
        }
    }

    public function testCreateFailUsernameAlreadyExists()
    {
        factory(User::class)->create(['username' => 'test']);
        $u = $this->updater->create([
            'username' => 'test',
            'email' => 'lala@lala.com',
            'password' => \Hash::make('lala')
        ]);
        // Check that Message bag is returned
        $this->assertTrue($u instanceof MessageBag);
        // Check that result ErrorBag has `username`
        $this->assertTrue($u->has('username'));
    }
}
