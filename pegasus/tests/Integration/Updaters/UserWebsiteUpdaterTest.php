<?php

namespace Tests\Integration\Validators;

use Tests\TestCase;
use App\Models\Users\User;
use App\Models\Users\UserWebsite;
use App\Models\Websites\Website;
use App\Updaters\UserWebsiteUpdater;
use Illuminate\Support\MessageBag;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserWebsiteUpdaterTest extends TestCase
{

    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->model = app(UserWebsite::class);
        $this->updater = app(UserWebsiteUpdater::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testCreateSuccess()
    {
        $u = factory(User::class)->create();
        $w = factory(Website::class)->create();
        $r = $this->updater->create(
            [
                'user_id' => $u->id,
                'website_id' => $w->id,
                'url' => 'http://test.com'
            ]
        );
        $this->assertTrue($r instanceof UserWebsite);
    }

    public function testCreateFail()
    {
        $u = factory(User::class)->create();
        $w = factory(Website::class)->create();
        $r = $this->updater->create(
            [
                'user_id' => $u->id,
                'website_id' => $w->id,
            ]
        );
        $this->assertTrue($r instanceof MessageBag);
        $this->assertTrue($r->has('url')); // Should have url
    }

    public function testUpdateByIdsSuccess()
    {
        $uw = factory(UserWebsite::class)->create();
        $r = $this->updater->updateByIds(
            $uw->user_id,
            $uw->website_id,
            'http://some.url'
        );
        $this->assertEquals(1, $r);
        $uw = $this->model
            ->where('user_id', $uw->user_id)
            ->where('website_id', $uw->website_id)
            ->first();
        $this->assertEquals('http://some.url', $uw->url);
    }

    public function testDeleteByIds()
    {
        $uw = factory(UserWebsite::class)->create();
        $r = $this->updater->deleteByIds(
            $uw->user_id,
            $uw->website_id
        );
        $this->assertEquals(1, $r);

        $this->assertEquals(
            0,
            $this->model
                ->where('user_id', $uw->user_id)
                ->where('website_id', $uw->website_id)
                ->count()
        );
    }
}
