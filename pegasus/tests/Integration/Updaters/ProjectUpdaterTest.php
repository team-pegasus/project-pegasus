<?php

namespace Tests\Integration\Validators;

use Tests\TestCase;
use App\Models\Users\User;
use App\Models\Projects\Project;
use App\Updaters\ProjectUpdater;
use Illuminate\Support\MessageBag;
use App\Events\Projects\NewProject;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProjectUpdaterTest extends TestCase
{

    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->updater = app(ProjectUpdater::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testCreateSuccess()
    {
        $this->expectsEvents(NewProject::class);
        $user = factory(User::class)->create();
        $r = $this->updater->create([
            'name' => 'test test',
            'description' => $this->strLen(250),
            'content' => $this->strLen(300)
        ], $user->id);
        $this->assertTrue($r instanceof Project);
    }

    public function testCreateFail()
    {
        $user = factory(User::class)->create();
        $r = $this->updater->create([
            'name' => 'test test',
            'description' => $this->strLen(100), // too short
            'content' => $this->strLen(300)
        ], $user->id);
        $this->assertTrue($r instanceof MessageBag);
    }

    public function testUpdateByIdSuccess()
    {
        $proj = factory(Project::class)->create();
        $r = $this->updater->updateById($proj, ['name' => 'test test']);
        $this->assertEquals('test-test', $proj->slug);
    }

    public function testUpdateByIdFail()
    {
        $proj = factory(Project::class)->create();
        $r = $this->updater->updateById($proj, ['name' => 'test']);
        $this->assertTrue($r instanceof MessageBag);
        $this->assertTrue($r->has('name'));
    }

    private function strLen($len)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $len; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
