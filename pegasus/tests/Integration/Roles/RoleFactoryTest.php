<?php

namespace Tests\Integration\Roles;

use Tests\TestCase;
use App\Lib\Roles\Role;
use App\Lib\Roles\RoleFactory;

class RoleFactoryTest extends TestCase
{

    protected $factory;
    protected $permissions;

    public function __construct()
    {
        parent::__construct();
        $this->factory = app()->make(RoleFactory::class);
        $this->permissions = ['messages.create', 'users.add', 'users.remove'];
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testMakeThread()
    {
        $r = $this->factory->makeThread('admin');
        $this->assertTrue($r instanceof Role);
        foreach ($this->permissions as $permission) {
            $this->assertTrue($r->hasPermission($permission));
        }
    }

    public function testMakeThreadFail()
    {
        $r = $this->factory->makeThread('for_sure_doesnt_exist');
        $this->assertEquals(0, $r->getPermissions()->count());
    }
}
