<?php

namespace Tests\Integration\Roles;

use Tests\TestCase;
use App\Lib\Roles\Permission;
use App\Lib\Roles\PermissionFactory;

class PermissionFactoryTest extends TestCase
{

    protected $factory;
    protected $permissions;

    public function __construct()
    {
        parent::__construct();
        $this->factory = app()->make(PermissionFactory::class);
        $this->permissions = ['users.index', 'users.view', 'users.create', 'users.update', 'users.delete'];
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testMake()
    {
        foreach ($this->permissions as $permission) {
            $r = $this->factory->makeGeneral($permission);
            $this->assertTrue($r instanceof Permission);
        }
    }

    public function testMakeMoreGeneral()
    {
        $r = $this->factory->makeManyGeneral($this->permissions);
        foreach ($this->permissions as $permission) {
            $this->assertTrue($r->search($permission) !== false);
        }
    }
}
