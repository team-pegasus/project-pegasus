<?php

namespace Tests\Integration\Repositories;

use Tests\TestCase;
use App\Models\Users\UserWebsite;
use App\Repos\UserWebsiteRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserWebsiteRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->repo = app(UserWebsiteRepository::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    /** Repo functions */

    public function testFindByIds()
    {
        $uw = factory(UserWebsite::class)->create();
        // Found
        $r = $this->repo->findByIds($uw->user_id, $uw->website_id);
        $this->assertTrue($r instanceof UserWebsite);
        // Not found
        $r = $this->repo->findByIds(-1, -1);
        $this->assertTrue($r === null);
    }
}
