<?php

namespace Tests\Integration\Repositories;

use DB;
use Carbon\Carbon;
use Tests\TestCase;
use App\Repos\CategoryRepository;
use App\Models\Categories\Category;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CategoryRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->repo = app(CategoryRepository::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testFindById()
    {
        factory(Category::class, 3)->create();
        $cat = factory(Category::class)->create();
        factory(Category::class, 3)->create();
        $r = $this->repo->findById($cat->id);
        $this->assertEquals($cat->getKey(), $r->getKey());
    }

    public function testFindBySlug()
    {
        factory(Category::class, 3)->create();
        $cat = factory(Category::class)->create();
        factory(Category::class, 3)->create();
        $r = $this->repo->findBySlug($cat->slug);
        $this->assertEquals($cat->getKey(), $r->getKey());
    }
}
