<?php

namespace Tests\Integration\Repositories;

use DB;
use Carbon\Carbon;
use Tests\TestCase;
use App\Models\Users\User;
use App\Repos\MessageRepository;
use App\Models\Messaging\Thread;
use App\Models\Messaging\Message;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MessageRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->repo = app(MessageRepository::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testGetMessagesAUserCanSeeFromThread()
    {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $thread = factory(Thread::class)->create();
        $thread->users()->sync([$user1->id, $user2->id]);

        // Creaate 2 messages by $user1 to $thread
        $first = factory(Message::class)->create([
                'user_id' => $user1->id,
                'thread_id' => $thread->id
            ]);
        $second = factory(Message::class)->create([
                'user_id' => $user1->id,
                'thread_id' => $thread->id,
                'created_at' => '2000-01-01 00:00:00'
            ]);

        // Assert that $user1 gets 2 messages (2 created here + 1 through setUp())
        // through getMessagesAUserCanSeeFromThread($user1->id, $thread->id)
        $this->assertEquals(2, $this->repo->getMessagesAUserCanSeeFromThread($user1->id, $thread->id)->total());

        // Remove $user2 from thread (by setting `left` to not null)
        DB::table('thread_user')->where('user_id', $user2->id)
            ->where('thread_id', $thread->id)->update(['left' => Carbon::now()]);
        // Assert that $user2 gets no messages through
        // getMessagesAUserCanSeeFromThread($user2->id, $thread->id)
        $this->assertEquals(0, $this->repo->getMessagesAUserCanSeeFromThread($user2->id, $thread->id)->total());

        // $user2 rejoins
        DB::table('thread_user')->where('user_id', $user2->id)
            ->where('thread_id', $thread->id)->update(['left' => null]);
        // Assert that $user2 now gets 3 messages as well
        // through getMessagesAUserCanSeeFromThread($user2->id, $thread->id)
        $this->assertEquals(2, $this->repo->getMessagesAUserCanSeeFromThread($user2->id, $thread->id)->total());

        // Random user
        $u = factory(User::class)->create();
        // ... should see no messages
        $this->assertEquals(0, $this->repo->getMessagesAUserCanSeeFromThread($u->id, $thread->id)->total());
    }
}
