<?php

namespace Tests\Integration\Repositories;

use Tests\TestCase;
use App\Repos\ThreadRepository;
use App\Models\Users\User;
use App\Models\Messaging\Thread;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ThreadRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->repo = app(ThreadRepository::class);
    }

    public function setUp()
    {
        parent::setup();
        factory(Thread::class, 3)->create();
    }

    public function testFindById()
    {
        $exp = factory(Thread::class)->create();
        $r = $this->repo->findById($exp->id);
        $this->assertEquals($exp->getKey(), $r->getKey());
    }

    public function testFindPrivateByUserIds()
    {
        $u1 = factory(User::class)->create();
        $u2 = factory(User::class)->create();
        $t = factory(Thread::class)->create();
        $t->participants()->sync([$u1->id, $u2->id]);

        $r = $this->repo->findPrivateByUserIds($u1->id, $u2->id);
        $this->assertNotNull($r);

        // Associate the thread with some model
        $t->threadable()->associate(factory(User::class)->create())->save();
        // Should no longer be found
        $r = $this->repo->findPrivateByUserIds($u1->id, $u2->id);
        $this->assertNull($r);
    }

    public function testGetByUserId()
    {
        $user = factory(User::class)->create();
        $exp = factory(Thread::class)->create();
        $exp->users()->sync([$user->id]);
        $r = $this->repo->getByUserId($user->id);
        $this->assertEquals($exp->getKey(), $r->first()->getKey());
    }
}
