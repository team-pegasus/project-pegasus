<?php

namespace Tests\Integration\Repositories;

use Tests\TestCase;
use App\Models\Projects\Project;
use App\Repos\ProjectRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProjectRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->repo = app(ProjectRepository::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    /** Repo functions */

    public function testFindById()
    {
        $p = factory(Project::class)->create();
        // Found
        $r = $this->repo->findById($p->id);
        $this->assertTrue($r instanceof Project);
        // Not found
        $r = $this->repo->findById(-1);
        $this->assertTrue($r === null);
    }

    public function testGetPaginated()
    {
        factory(Project::class, 5)->create();
        $r = $this->repo->getPaginated(2);
        // Should have 2 results
        $this->assertEquals(2, $r->count());
        // Should have 3 pages
        $this->assertEquals(3, $r->lastPage());
    }
}
