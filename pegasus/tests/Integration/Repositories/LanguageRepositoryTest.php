<?php

namespace Tests\Integration\Repositories;

use DB;
use Carbon\Carbon;
use Tests\TestCase;
use App\Repos\LanguageRepository;
use App\Models\Categories\Language;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LanguageRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->repo = app(LanguageRepository::class);
    }

    public function setUp()
    {
        parent::setup();
        factory(Language::class, 3)->create();
    }

    public function testFindById()
    {
        $expected = factory(Language::class)->create();
        $r = $this->repo->findById($expected->id);
        $this->assertEquals($expected->getKey(), $r->getKey());
    }

    public function testFindBySlug()
    {
        $expected = factory(Language::class)->create();
        $r = $this->repo->findBySlug($expected->slug);
        $this->assertEquals($expected->getKey(), $r->getKey());
    }

    public function testGetAll()
    {
        factory(Language::class)->create();
        $expCount = DB::table('languages')->count();
        $r = $this->repo->getAll();
        $this->assertEquals($expCount, $r->count());
    }
}
