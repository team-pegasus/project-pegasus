<?php

namespace Tests\Integration\Repositories;

use Tests\TestCase;
use App\Models\Users\User;
use App\Repos\UserRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->repo = app(UserRepository::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    /** Repo functions */

    public function testFindById()
    {
        $p = factory(User::class)->create();
        // Found
        $r = $this->repo->findById($p->id);
        $this->assertTrue($r instanceof User);
        // Not found
        $r = $this->repo->findById(-1);
        $this->assertNull($r);
    }

    public function testFindByEmail()
    {
        $p = factory(User::class)->create();
        // Found
        $r = $this->repo->findByEmail($p->email);
        $this->assertTrue($r instanceof User);
        // Not found
        $r = $this->repo->findByEmail(-1);
        $this->assertNull($r);
    }

    public function testFindByUsername()
    {
        $p = factory(User::class)->create();
        // Found
        $r = $this->repo->findByUsername($p->username);
        $this->assertTrue($r instanceof User);
        // Not found
        $r = $this->repo->findByUsername(null);
        $this->assertNull($r);
    }

    public function testFindBySocialId()
    {
        $p = factory(User::class)->create(['github_id' => 'test']);
        // Found
        $r = $this->repo->findBySocialId('github', $p->github_id);
        $this->assertTrue($r instanceof User);
        // Not found
        $r = $this->repo->findBySocialId('github', -1);
        $this->assertNull($r);
    }

    public function testGetPaginated()
    {
        factory(User::class, 5)->create();
        $r = $this->repo->getPaginated(2);
        // Should have 2 results
        $this->assertEquals(2, $r->count());
        // Should have 3 pages
        $this->assertEquals(3, $r->lastPage());
    }

    public function testGetAll()
    {
        factory(User::class, 5)->create();
        $r = $this->repo->getAll();
        // Should have 5 results
        $this->assertEquals(5, $r->count());
    }
}
