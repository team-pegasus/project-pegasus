<?php

namespace Tests\Integration\Validators;

use Tests\TestCase;
use Illuminate\Support\MessageBag;
use App\Lib\Validators\UserValidator;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserValidatorTest extends TestCase
{

    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->validator = app(UserValidator::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testCreate()
    {
        // Fail
        $errs = $this->validator->create([]);
        $this->assertTrue($errs instanceof MessageBag);
        $this->assertTrue($errs->has('username'));
        $this->assertTrue($errs->has('email'));

        // Success
        $errs = $this->validator->create([
            'email' => 'lala@lala.com',
            'github_id' => 'sth',
        ]);
        $this->assertTrue($errs);
    }

    public function testUpdate()
    {
        $errs = $this->validator->update(
            [
                'github_id' => 'sth'
            ],
            [
                'github_id' => 'sth else'
            ]
        );
        $this->assertTrue($errs instanceof MessageBag);
        $this->assertTrue($errs->has('github_id'));
    }
}
