<?php

namespace Tests\Integration\Validators;

use Tests\TestCase;
use Illuminate\Support\MessageBag;
use App\Lib\Validators\ProjectValidator;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProjectValidatorTest extends TestCase
{

    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->validator = app(ProjectValidator::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testCreate()
    {
        $errs = $this->validator->create([]);
        $this->assertTrue($errs instanceof MessageBag);
        $this->assertTrue($errs->has('name'));
        $this->assertTrue($errs->has('description'));
        $this->assertTrue($errs->has('content'));
    }

    public function testUpdate()
    {
        $errs = $this->validator->update(['name' => 'short']);
        $this->assertTrue($errs instanceof MessageBag);
        $this->assertTrue($errs->has('name'));

        $errs = $this->validator->update([]);
        $this->assertTrue($errs);
    }
}
