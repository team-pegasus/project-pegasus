<?php

namespace Tests\Integration\Validators;

use Tests\TestCase;
use App\Models\Users\UserWebsite;
use Illuminate\Support\MessageBag;
use App\Lib\Validators\UserWebsiteValidator;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserWebsiteValidatorTest extends TestCase
{

    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->validator = app(UserWebsiteValidator::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testCreate()
    {
        $errs = $this->validator->create([]);
        $this->assertTrue($errs instanceof MessageBag);
        $this->assertTrue($errs->has('user_id'));
        $this->assertTrue($errs->has('website_id'));
        $this->assertTrue($errs->has('url'));

        $new = factory(UserWebsite::class)->create();
        $errs = $this->validator->create($new->getAttributes());
        $this->assertTrue($errs instanceof MessageBag);
        $this->assertTrue($errs->has('website_id'));
    }
}
