<?php

namespace Tests\Integration\Validators;

use Tests\TestCase;
use Illuminate\Support\MessageBag;
use App\Lib\Validators\MessageValidator;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MessageValidatorTest extends TestCase
{

    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->validator = app(MessageValidator::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testCreate()
    {
        // Fail
        $errs = $this->validator->create([]);
        $this->assertTrue($errs instanceof MessageBag);
        $this->assertTrue($errs->has('content'));

        // Success
        $errs = $this->validator->create([
            'content' => 'sth',
        ]);
        $this->assertTrue($errs);
    }
}
