<?php

namespace Tests\Integration\Validators;

use DB;
use Tests\TestCase;
use App\Models\Users\User;
use App\Models\Messaging\Thread;
use Illuminate\Support\MessageBag;
use App\Lib\Validators\ThreadValidator;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ThreadValidatorTest extends TestCase
{

    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->validator = app(ThreadValidator::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testCreate()
    {
        $u1 = factory(User::class)->create();
        $u2 = factory(User::class)->create();
        $r = $this->validator->create([
            'last_message' => 'sth'
        ], [$u1->id, $u2->id]);
        $this->assertTrue($r instanceof MessageBag);
        $this->assertTrue($r->has('last_message'));

        $r = $this->validator->create([
            'last_message' => null
        ], [$u1->id, $u2->id]);
        $this->assertTrue($r);

        // Assert that a private thread cant be created
        // twice with same users
        $u1 = factory(User::class)->create();
        $u2 = factory(User::class)->create();
        $t1 = factory(Thread::class)->create();
        $t1->participants()->sync([$u1->id, $u2->id]);

        $this->assertTrue($this->validator->create([], [$u1->id, $u2->id]) instanceof MessageBag);
    }

    public function testAddUserToThread()
    {
        $user = factory(User::class)->create();
        $thread = factory(Thread::class)->create();

        // Fake private thread, should fail cause private
        $errs = $this->validator->addUserToThread($thread->id, $user->id);
        $this->assertTrue($errs instanceof MessageBag);
        $this->assertTrue($errs->has('private'));

        // Associate it with a random model (no longer private)
        $thread->threadable()->associate(factory(User::class)->create())->save();

        // Add $user to the thread by cheating
        DB::table('thread_user')
            ->insert(['thread_id' => $thread->id, 'user_id' => $user->id]);

        // Should fail adding the user because he's already in
        $errs = $this->validator->addUserToThread($thread->id, $user->id);
        $this->assertTrue($errs instanceof MessageBag);
        $this->assertTrue($errs->has('unique'));

        // Should succeed
        $user = factory(User::class)->create();
        $thread = factory(Thread::class)->create();
        $thread->threadable()->associate(factory(User::class)->create())->save();

        $errs = $this->validator->addUserToThread($thread->id, $user->id);
        $this->assertTrue($errs);
    }
}
