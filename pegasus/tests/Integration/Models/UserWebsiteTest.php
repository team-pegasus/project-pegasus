<?php

namespace Tests\Integration\Models;

use DB;
use Carbon\Carbon;
use Tests\TestCase;
use App\Models\Users\User;
use App\Models\Users\UserWebsite;
use App\Models\Websites\Website;
use App\Repos\UserWebsiteRepository;
use App\Updaters\UserWebsiteUpdater;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserWebsiteTest extends TestCase
{
    use DatabaseTransactions;

    private $thread;

    public function __construct()
    {
        parent::__construct();
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testUserRelationship()
    {
        $uw1 = factory(UserWebsite::class)->create();
        // Assert that $user1 is the `user` (name of relationship) of $uw1
        $this->assertTrue($uw1->user() instanceof BelongsTo);
    }

    public function testWebsiteRelationship()
    {
        $github = factory(Website::class)->create(['key' => 'github']);
        $rnd = factory(Website::class)->create(['key' => 'rnd']);
        $user = factory(User::class)->create();
        $uw1 = factory(UserWebsite::class)
            ->create(
                [
                    'user_id' => $user->id,
                    'website_id' => $github->id
                ]
            );
        $uw2 = factory(UserWebsite::class)
            ->create(
                [
                    'user_id' => $user->id,
                    'website_id' => $rnd->id
                ]
            );

        $this->assertTrue($uw2->website() instanceof BelongsTo);
        // Assert that $github is the `website` (name of relationship) of $uw1
        $this->assertEquals($github->getKey(), $uw1->website->getKey());
        // Assert that random website is not found
        $this->assertEquals(
            null,
            $uw2->website()->where(
                'key',
                $rnd->key
            )->first()
        );
    }
}
