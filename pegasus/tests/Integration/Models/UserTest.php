<?php

namespace Tests\Integration\Models;

use Tests\TestCase;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    protected $model;

    public function __construct()
    {
        parent::__construct();

        $this->model = app()->make(User::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    /** Relationships */
    public function testRolesRelation()
    {
        $u = factory(User::class)->create();
        $this->assertTrue($u->roles() instanceof HasMany);
    }

    public function testThreadRelation()
    {
        $u = factory(User::class)->create();
        $this->assertTrue($u->threads() instanceof BelongsToMany);
    }

    // Todo test correct scopes
    public function testWebsiteLinksRelation()
    {
        $u = factory(User::class)->create();
        $this->assertTrue($u->websiteLinks() instanceof HasMany);
    }

    /** Scopes */
    public function testScopeByEmail()
    {
        $u = factory(User::class)->create();
        $r = $this->model->byEmail($u->email)->first();
        $this->assertEquals($u->getKey(), $r->getKey());
    }

    public function testScopeByUsername()
    {
        $u = factory(User::class)->create();
        $r = $this->model->byUsername($u->username)->first();
        $this->assertEquals($u->getKey(), $r->getKey());
    }

    public function testScopeBySocialId()
    {
        $u = factory(User::class)->create(['github_id' => 'test123']);
        $r = $this->model->bySocialId('github', $u->github_id)->first();
        $this->assertEquals($u->getKey(), $r->getKey());
    }
}
