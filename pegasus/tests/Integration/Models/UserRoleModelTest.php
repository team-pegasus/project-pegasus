<?php

namespace Tests\Integration\Models;

use Tests\TestCase;
use App\Models\Roles\UserRole;

class UserRoleModelTest extends TestCase
{
    public function __construct()
    {
        parent::__construct();
        $this->userRole = new UserRole();
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testGetPermissions()
    {
        $userRole = $this->userRole;
        $userRole->role = 'admin';
        $this->assertEquals($userRole->getPermissions()->count(), collect(config('roles.admin'))->count());
        $this->assertEquals($userRole->getPermissions()->first()->getName(), collect(config('roles.admin'))->first());
    }

    public function testHasPermission()
    {
        $userRole = $this->userRole;
        $userRole->role = 'admin';
        $this->assertTrue($userRole->hasPermission('users.index'));
    }

    public function testHasPermissionFail()
    {
        $userRole = $this->userRole;
        $userRole->role = 'admin';
        $this->assertFalse($userRole->hasPermission('doesnt.exist.at.all'));
    }
}
