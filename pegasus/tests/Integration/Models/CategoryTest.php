<?php

namespace Tests\Integration\Models;

use Tests\TestCase;
use App\Repos\CategoryRepository;
use App\Models\Categories\Category;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CategoryTest extends TestCase
{
    use DatabaseTransactions;

    protected $parent1;
    protected $child1;

    public function __construct()
    {
        parent::__construct();
    }

    public function setUp()
    {
        parent::setup();

        $this->parent1 = factory(Category::class)->create([
            'level' => 0,
            'name' => 'Test 1',
            'slug' => 'test-1',
            'description' => 'Really short description'
        ]);
        $this->parent2 = factory(Category::class)->create([
            'level' => 0,
            'name' => 'Test 2',
            'slug' => 'test-2',
            'description' => 'Second short description'
        ]);
        $this->parent3 = factory(Category::class)->create([
            'level' => 0,
            'name' => 'Test 3',
            'slug' => 'test-3',
            'description' => 'Third short description'
        ]);
        // Children
        $this->child1 = factory(Category::class)->create([
            'level' => 1,
            'name' => 'Sub 1',
            'slug' => 'Sub 1',
            'description' => 'Short sub description'
        ]);

        $this->parent1->children()->attach($this->child1);
    }

    /** Model functions */

    public function testChildren()
    {
        $expected = $this->child1;

        $results = $this->parent1->children()->first();

        $this->assertEquals($expected->getKey(), $results->getKey());
    }

    public function testDirectChildren()
    {
        $expected = $this->child1;

        $results = $this->parent1->directChildren()->first();

        $this->assertEquals($expected->getKey(), $results->getKey());

        $resultsCount = $this->child1->directChildren()->count();

        $this->assertEquals($resultsCount, 0);
    }

    public function testParents()
    {
        $expected = $this->parent1;

        $results = $this->child1->parents()->first();

        $this->assertEquals($expected->getKey(), $results->getKey());
    }

    public function testDirectParents()
    {
        $expected = $this->parent1;

        $results = $this->child1->directParents()->first();

        $this->assertEquals($expected->getKey(), $results->getKey());

        $resultsCount = $this->parent1->directParents()->count();

        $this->assertEquals($resultsCount, 0);
    }
}
