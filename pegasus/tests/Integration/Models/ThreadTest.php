<?php

namespace Tests\Integration\Models;

use DB;
use Carbon\Carbon;
use Tests\TestCase;
use App\Models\Users\User;
use App\Repos\ThreadRepository;
use App\Updaters\ThreadUpdater;
use App\Models\Messaging\Thread;
use App\Models\Messaging\Message;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ThreadTest extends TestCase
{
    use DatabaseTransactions;

    protected $thread;
    protected $repo;
    protected $updater;

    // Empty thread
    protected $emptyT;

    public function __construct()
    {
        parent::__construct();
        $this->thread = app()->make(Thread::class);
        $this->repo = app()->make(ThreadRepository::class);
        $this->updater = app()->make(ThreadUpdater::class);
    }

    public function setUp()
    {
        parent::setup();

        $this->emptyT = factory(Thread::class)->create();
    }

    public function testUsersRelationship()
    {
        $this->assertTrue($this->emptyT->users() instanceof BelongsToMany);
        // Attach a user and make sure it exists
        $user = factory(User::class)->create();
        $this->emptyT->users()->attach($user->id);
        $this->assertNotNull($this->emptyT->users()->where('id', $user->id)->first());
    }

    public function testMessagesRelationship()
    {
        $this->assertTrue($this->emptyT->messages() instanceof HasMany);
        // Attach a message and make sure it exists
        $message = factory(Message::class)->create([
            'thread_id' => $this->emptyT->id,
        ]);
        $this->assertNotNull($this->emptyT->messages()->where('id', $message->id)->first());
    }

    public function testThreadableRelationship()
    {
        $this->assertTrue($this->emptyT->threadable() instanceof MorphTo);
        // Attach a user and make sure it exists
        $user = factory(User::class)->create();
        $this->assertTrue($this->emptyT->threadable()->associate($user)->save());
    }

    public function testScopeOrder()
    {
        // Factory 2 new threads with different `last_message` timestamps
        $first = factory(Thread::class)->create([
                'last_message' => '2017-08-26 00:00:00'
            ]);
        $second = factory(Thread::class)->create([
                'last_message' => '2017-08-25 00:00:00'
            ]);
        // Fetch with scope ->order()
        $result = $this->thread->order()->get();
        // Loop till pre last one
        for ($i = 0; $i < $result->count() - 2; $i++) {
            if ($result[$i] == null) {
                continue;
            }
            // Check that current thread has bigger `last_message` timestamp than next
            $this->assertGreaterThanOrEqual($result[$i + 1]->last_message, $result[$i]->last_message, "Item i = $i is smaller than i + 1, shouldnt be");
        }
    }

    public function testScopePrivate()
    {
        // Create a thread that doesnt have a threadable
        $t = factory(Thread::class)->create();
        // Assert that it's found with `private()` scope
        $this->assertNotNull(Thread::private()->find($t->id));
        // Attach a generated user as threadable
        $t->threadable()->associate(factory(User::class)->create())->save();
        // Assert that it is no longer found with `private()` scope
        $this->assertNull(Thread::private()->find($t->id));
    }

    public function testAddUserToThread()
    {
        $emptyT = factory(Thread::class)->create();
        // Test on a non-private thread
        $emptyT->threadable()->associate(factory(User::class)->create())->save();
        // Create random user (0 threads)
        $user = factory(User::class)->create();
        // Add new $user to $emptyT
        $this->updater->addUserToThread($emptyT->id, $user->id);
        // Assert that $emptyT has $user
        $this->assertEquals(1, $this->thread->byUserId($user->id)->get()->count());
        // Remove user from thread
        DB::table('thread_user')->where([
                'thread_id' => $emptyT->id,
                'user_id' => $user->id
            ])->update(['left' => Carbon::now()]);
        // Assert that $emptyT doesnt have $user
        $this->assertEquals(0, $this->thread->byUserId($user->id)->get()->count());
        // Add previously removed $user to $emptyT
        $this->updater->addUserToThread($emptyT->id, $user->id);
        // Assert that $emptyT has $user again
        $this->assertEquals(1, $this->thread->byUserId($user->id)->get()->count());
    }

    public function testRemoveUserFromThread()
    {
        // Create random user (0 threads)
        $user = factory(User::class)->create();
        // Add new $user to $emptyT
        $user->threads()->attach($this->emptyT->id);

        // Assert that $emptyT has 1 user
        $this->assertEquals(1, $this->thread->byUserId($user->id)->get()->count());
        // Remove $user from $emptyT
        $this->updater->removeUserFromThread($this->emptyT->id, $user->id);
        // Assert that $emptyT has 0 users
        $this->assertEquals(0, $this->thread->byUserId($user->id)->get()->count());
    }
}
