<?php

namespace Tests\Integration\Models;

use Tests\TestCase;
use App\Models\Websites\Website;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WebsiteTest extends TestCase
{
    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();
        $this->website = app(Website::class);
    }

    public function setUp()
    {
        parent::setup();

        $this->github = factory(Website::class)
            ->create([
                'key' => 'github',
            ]);
        $this->custom = factory(Website::class)
            ->create([
                'key' => 'custom',
            ]);
        $this->random = factory(Website::class)->create();
    }

    public function testScopeAUserCanHave()
    {
        $this->assertEquals(2, $this->website->aUserCanHave()->count());
    }
}
