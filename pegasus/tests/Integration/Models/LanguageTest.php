<?php

namespace Tests\Integration\Models;

use Tests\TestCase;
use App\Models\Categories\Language;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LanguageTest extends TestCase
{
    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();
    }

    public function setUp()
    {
        parent::setup();
        factory(Language::class, 3)->create();
    }

    public function testScopeBySlug()
    {
        $exp = factory(Language::class)->create();
        $withSlug = Language::bySlug($exp->slug)->get();
        foreach ($withSlug as $l) {
            $this->assertEquals($exp->slug, $l->slug);
        }
    }

    public function testScopeByName()
    {
        $exp = factory(Language::class)->create();
        $withSlug = Language::byName($exp->name)->get();
        foreach ($withSlug as $l) {
            $this->assertEquals($exp->name, $l->name);
        }
    }
}
