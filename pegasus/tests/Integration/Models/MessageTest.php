<?php

namespace Tests\Integration\Models;

use DB;
use Carbon\Carbon;
use Tests\TestCase;
use App\Models\Users\User;
use App\Repos\MessageRepository;
use App\Updaters\MessageUpdater;
use App\Models\Messaging\Thread;
use App\Models\Messaging\Message;
use App\Events\Messaging\NewMessage;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MessageTest extends TestCase
{
    use DatabaseTransactions;

    private $thread;

    public function __construct()
    {
        parent::__construct();
        $this->model = app()->make(Message::class);
        $this->repo = app()->make(MessageRepository::class);
        $this->updater = app()->make(MessageUpdater::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    public function testOwnerRelationship()
    {
        $message = factory(Message::class)->create();
        // Assert that owner is instance of BelongsTo relationship
        $this->assertTrue($message->owner() instanceof BelongsTo);
    }

    public function testThreadRelationship()
    {
        $message = factory(Message::class)->create();
        // Assert that thread() is instance of BelongsTo relationship
        $this->assertTrue($message->thread() instanceof BelongsTo);
    }

    public function testScopeOrder()
    {
        $thread = factory(Thread::class)->create();
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $thread->users()->sync([$user1->id, $user2->id]);
        $message = factory(Message::class)->create([
                'user_id' => $user1->id,
                'thread_id' => $thread->id
            ]);
        // Factory 2 new messages with different `last_message` timestamps
        // they belong to $user1
        $first = factory(Message::class)->create([
                'user_id' => $user1->id,
                'thread_id' => $thread->id
            ]);
        $second = factory(Message::class)->create([
                'user_id' => $user1->id,
                'thread_id' => $thread->id,
                'created_at' => '2000-01-01 00:00:00'
            ]);

        // Fetch with scope ->order()
        $result = $this->model->order()->get();
        // Loop till pre last one
        for ($i = 0; $i < $result->count() - 2; $i++) {
            if ($result[$i] == null) {
                continue;
            }
            // Check that current message has bigger `created_at` timestamp than next
            $this->assertGreaterThanOrEqual($result[$i + 1]->created_at, $result[$i]->created_at);
        }
    }
}
