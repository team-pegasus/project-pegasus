<?php

namespace Tests\Integration\Models;

use Tests\TestCase;
use App\Models\Users\User;
use App\Models\Messaging\Thread;
use App\Models\Projects\Project;
use App\Models\Categories\Language;
use App\Lib\Traits\Models\Languagable;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProjectTest extends TestCase
{
    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $this->model = app(Project::class);
    }

    public function setUp()
    {
        parent::setup();
    }

    /** Model functions */

    public function testUserRelationship()
    {
        // Assert new project has user
        $p = factory(Project::class)->create();
        $this->assertTrue($p->user() instanceof BelongsTo);
        $u = factory(User::class)->create();
        $p->user_id = $u->id;
        $p->load('user');
        $this->assertEquals($u->getKey(), $p->user->getKey());
    }

    public function testLanguableTrait()
    {
        $p = factory(Project::class)->create();
        $this->assertTrue($p->languages() instanceof MorphToMany);
    }

    public function testThreadRelation()
    {
        $p = factory(Project::class)->create();
        $this->assertTrue($p->thread() instanceof MorphOne);
        // Add a thread to a project and check that the project finds
        // it through a relationship
        $t = factory(Thread::class)->create();
        $t->threadable()->associate($p)->save();
        $p->load(['thread']);
        $this->assertNotNull($p->thread);
    }
}
