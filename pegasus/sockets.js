/* eslint-disable new-cap */
let server = require('http').Server();

let io = require('socket.io')(server);

let Redis = require('ioredis');
let redis = new Redis();

// Channel name (TODO: take from .env)
redis.subscribe('pegasus');
/**
 * Message looks like this:
 * {
 *   event: string event name,
 *   data: json payload
 * }
 */
redis.on('message', function(channel, message) {
  message = JSON.parse(message);
  // Publish to client
  io.emit(channel + ':' + message.event, message.data);
  console.log(channel, message);
});

server.listen(3000);
