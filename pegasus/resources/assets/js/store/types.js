export const SHOW_MESSAGE = 'SHOW_MESSAGE';
export const HIDE_MESSAGE = 'HIDE_MESSAGE';

// login & user profile
export const SET_USER = 'SET_USER';
export const SET_PROFILE = 'SET_PROFILE';
export const LOGIN_ERRORS = 'LOGIN_ERRORS';
export const LOGOUT = 'LOGOUT';
export const LOAD_USERS = 'LOAD_USERS';

// messaging
export const SWITCH_THREAD = 'SWITCH_THREAD';
// creating and receiving messages
export const THREADS_LOADED = 'THREADS_LOADED';
export const NEW_MESSAGE = 'NEW_MESSAGE';
export const NEW_THREAD = 'NEW_THREAD';

// Projects
export const SET_PROJECTS = 'SET_PROJECTS';
export const SET_PROJECT = 'SET_PROJECT';


// message types
export const THREAD_TYPE_USER = 'USER';
