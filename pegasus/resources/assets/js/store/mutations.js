import {
  SHOW_MESSAGE,
  HIDE_MESSAGE,
  SET_PROFILE,
  LOAD_USERS,
} from './types';

export const mutations = {

  [SHOW_MESSAGE](state, content, type) {
    console.log('todo' + content);
  },

  [HIDE_MESSAGE](state, content, type) {
    console.log('todo' + content);
  },

  [SET_PROFILE](state, user) {
    state.profile = user;
  },

  [LOAD_USERS](state, users) {
    state.users = users;
  },
};
