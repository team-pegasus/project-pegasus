import {
  SHOW_MESSAGE,
  HIDE_MESSAGE,
  SET_PROFILE,
  LOAD_USERS,
} from './types';
import users from './../api/users.js';

export const actions = {

  showMessage({commit}, content, type = 'error') {
    commit(SHOW_MESSAGE, {content: content, type: type});
  },

  hideMessage({commit}) {
    commit(HIDE_MESSAGE);
  },

  findUserProfile({commit}, id) {
    return new Promise((resolve, reject) => {
      users.find(id)
        .then((res) => {
          commit(SET_PROFILE, res.data);
          resolve(res);
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  },

  getUsers({commit}) {
    return new Promise((resolve, reject) => {
      users.get()
        .then(res => {
          commit(LOAD_USERS, res.data.data);
        })
        .catch((err) => {
          console.error(err);
        });
    });
  },

};
