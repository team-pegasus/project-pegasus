import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';
import auth from './modules/auth';
import messages from './modules/messages';
import projects from './modules/projects';
import {actions} from './actions';
import {getters} from './getters';
import {mutations} from './mutations';
import {state} from './state';

const debug = process.env.NODE_ENV !== 'production';

Vue.use(Vuex);

const store = new Vuex.Store({
  state,
  actions,
  state,
  getters,
  mutations,
  modules: {
    auth: auth,
    messages: messages,
    projects: projects,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : [],
});

if (module.hot) {
  module.hot.accept([
    './getters',
    './actions',
    './mutations',
    './modules/auth',
    './modules/messages',
  ], () => {
    // require the updated modules
    // have to add .default here due to babel 6 module output
    const newModuleAuth = require('./modules/auth').default;
    const newModuleMessages = require('./modules/messages').default;
    // swap in the new actions and mutations
    store.hotUpdate({
      getters: require('./getters'),
      actions: require('./actions'),
      mutations: require('./mutations'),
      modules: {
        auth: newModuleAuth,
        messages: newModuleMessages,
      },
    });
  });
};

export default store;
