import usersApi from '../../api/users';
import router from '../../routes';
import {
  SET_USER,
  LOGOUT,
  SHOW_MESSAGE,
} from '../types';

let loginMessage = 'Login successful';

const state = {
  // Initial user is encoded in meta tag with id #meta-current-user
  user: JSON.parse(
    document.getElementById('meta-current-user')
      .getAttribute('content')
  ),
  errors: {},
};

const getters = {

  user(state) {
    return state.user;
  },

  username(state) {
    if (state.user.username) {
      return state.user.username;
    }
    return state.user.email;
  },

  loggedIn(state) {
    // Check if user object is empty
    return !(Object.keys(state.user).length === 0
      && state.user.constructor === Object);
  },

  errors(state) {
    return state.errors;
  },

};

const mutations = {

  [SET_USER](state, user) {
    state.user = user;
    if (router.currentRoute.name === 'login') {
      // Redirect home
      router.push({name: 'home'});
    }
  },

  [LOGOUT](state) {
    state.user = {};
  },

};

const actions = {

  login({commit}, credentials) {
    return new Promise((resolve, reject) => {
      usersApi.login(credentials)
        .then((res) => {
          // Set user data
          commit(SET_USER, res.data);
          commit(SHOW_MESSAGE, loginMessage);
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  logout({commit}) {
    window.location.href = '/logout';
    commit(LOGOUT);
  },

  register({commit}, credentials) {
    return new Promise((resolve, reject) => {
      usersApi.register(credentials)
        .then((res) => {
          // Set user data
          commit(SET_USER, res.data);
          commit(SHOW_MESSAGE, loginMessage);
          // Resolve promise, so parents can still use it
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

};

export default {
  state,
  actions,
  getters,
  mutations,
};
