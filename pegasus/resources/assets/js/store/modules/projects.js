import projectsApi from '../../api/projects';

import {
  SET_PROJECTS,
  SET_PROJECT,
} from '../types';

console.log('SET_PROJECTS', SET_PROJECTS);

const state = {
  project: {},
  projects: [],
};

const getters = {

  project() {
    return state.project;
  },

  projects(state) {
    return state.projects;
  },

};

const mutations = {

  [SET_PROJECTS](state, {data}) {
    state.projects = data;
  },

  [SET_PROJECT](state, data) {
    state.project = data;
  },

};

const actions = {

  getProject({commit}, id) {
    return new Promise((resolve, reject) => {
      projectsApi.getProject(id)
        .then(res => {
          commit(SET_PROJECT, res.data);
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  getProjects({commit}) {
    return new Promise((resolve, reject) => {
      projectsApi.getProjects()
        .then((res) => {
          commit(SET_PROJECTS, res.data);
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  createProject({commit}, project) {
    return new Promise((resolve, reject) => {
      projectsApi.createProject(project)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  editProject({commit}, project) {
    return new Promise((resolve, reject) => {
      projectsApi.editProject(project.id, project)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  },

};

export default {
  state,
  actions,
  getters,
  mutations,
};
