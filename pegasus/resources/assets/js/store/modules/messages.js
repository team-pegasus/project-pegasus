import messagesApi from '../../api/messages';
import moment from 'moment';

import {
  NEW_MESSAGE,
  NEW_THREAD,
  SWITCH_THREAD,
  THREADS_LOADED,
} from '../types';


const state = {
  currentThreadID: null,
  threads: [],
  threadsLoaded: false,
};

const getters = {
  /**
   * Find thread with given id
   * @param  {int} state - id
   * @return {obj} thread
   */
  thread: (state) => (id) => {
    return state.threads.find(el => el.id === id);
  },

  threadsLoaded: (state) => {
    return state.threadsLoaded;
  },

  threadTypeParticipants: (state) => (type, participantsIds) => {
    // find thread or null
    return state.threads.find(t => {
      // participants length exactly as given
      return t.participants.length === participantsIds.length &&
        // all ids in participants exist in given array
        t.participants.every(p => {
          return participantsIds.includes(p.id);
        }) &&
        t.type === type;
    });
  },

  /* Get all threads */
  threads(state) {
    return state.threads
      .slice()
      .sort((a, b) => {
        a = a.lastMessage.created_at || 0;
        b = b.lastMessage.created_at || 0;
        return b - a;
      });
  },

  currentThread(state, getters) {
    let ct = getters.thread(state.currentThreadID);

    return ct || {
      name: 'Thread was not selected',
      messages: [],
      id: -1,
      participants: [],
    };
  },

  currentThreadMessages(state, getters) {
    if (state.currentThreadID) {
      return getters.currentThread.messages
        .slice()
        .sort((a, b) => a.created_at - b.created_at);
    }

    return [];
  },

  lastMessageTime(sate, getters) {
    let lastTime = moment().subtract(50, 'years');

    // iterate over all messages and get last time
    state.threads.forEach(t => {
      lastTime = Math.max(lastTime, t.lastMessage.created_at);
    });

    return lastTime;
  },
};

const mutations = {

  [NEW_MESSAGE](state, message) {
    message.isRead = message.thread_id === state.currentThreadID;
    let thread = state.threads.find(t => t.id === message.thread_id);
    if (!thread.messages.some(m => m.id === message.id)) {
      thread.lastMessage = message;
      thread.messages.push(message);
    }
  },

  [SWITCH_THREAD](state, id) {
    state.currentThreadID = id;
  },

  [NEW_THREAD](state, thread) {
    if (!state.threads.some(t => t.id === thread.id)) {
      // set last message
      if (thread.messages.length > 0) {
        thread.lastMessage = thread.messages.slice().pop();
      } else {
        thread.lastMessage = {};
      }

      // store
      state.threads.push(thread);
    }
  },

  [THREADS_LOADED](state) {
    state.threadsLoaded = true;
  },
};

const actions = {
  getThreads({commit}) {
    return new Promise((resolve, reject) => {
      messagesApi.getThreads()
        .then((res) => {
          res.data.forEach(t => {
            commit(NEW_THREAD, t);
            commit(SWITCH_THREAD, t.id);
          });
          commit(THREADS_LOADED);
          resolve(res.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  sendMessage({commit}, message) {
    return new Promise((resolve, reject) => {
      messagesApi.sendMessage(message)
        .then((res) => {
          commit(NEW_MESSAGE, res.data);
          resolve(res.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // receiveThread({commit}, payload) {
  //   // use when another guy creates thread and invites you in
  //   this.$store.dispatch('createThread', payload);
  // },

  switchThread({commit}, id) {
    commit(SWITCH_THREAD, id);
  },

  createThread({commit, state, getters, dispatch}, {type, id}) {
    let participants = [id, getters.user.id];

    return new Promise((resolve, reject) => {
      let thread = getters.threadTypeParticipants(type, participants);
      if (thread) {
        console.log(thread);
        commit(SWITCH_THREAD, thread.id);
        resolve();
        return;
      }
      // check if threads have been loaded already
      if (!state.threadsLoaded) {
        // otherwise we need to load them
        dispatch('getThreads')
          .then((res) => {
            dispatch('createThread', {type, id});
            commit(THREADS_LOADED); // 100% don't fall in endless recursion
          });
      } else {
        // TODO: accept array of participants at backend endpoint
        messagesApi.createThread({type, id})
          .then((res) => {
            commit(NEW_THREAD, res.data);
            commit(SWITCH_THREAD, res.data.id);
            resolve(res.data);
          })
          .catch((err) => {
            console.warn(err);
            // showMessage({commit}, 'Couldnt send message');
            reject(err);
          });
      }
      resolve();
    });
  },
};

export default {
  state,
  actions,
  getters,
  mutations,
};
