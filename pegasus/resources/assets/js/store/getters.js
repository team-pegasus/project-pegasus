export const getters = {

  profile(state) {
    return state.profile;
  },
  users(state) {
    return state.users;
  },
};
