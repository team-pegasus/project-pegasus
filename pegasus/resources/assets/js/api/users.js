import axios from 'axios';
import {
  LOGIN_URL,
  LOGOUT_URL,
  REGISTER_URL,
  PASSWORD_RESET_URL,
  GET_USER,
} from './endpoints';

export default {
  // Logs in with passed credentials
  login(creds) {
    return axios.post(LOGIN_URL, creds);
  },
  // Logs out
  logout() {
    return axios.get(LOGOUT_URL);
  },
  // Registers a new user with passed credentials
  register(creds) {
    return axios.post(REGISTER_URL, creds);
  },
  // Emails the given email their password reset token
  emailPasswordResetToken(data) {
    return axios.post(PASSWORD_RESET_URL, data);
  },
  // Get other user
  find(id) {
    return axios.get(GET_USER + id);
  },
  get() {
    return axios.get(GET_USER);
  },
};
