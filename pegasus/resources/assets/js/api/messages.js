import axios from 'axios';
import {vsprintf} from 'sprintf-js';
import {
  ALL_THREADS,
  CREATE_MESSAGE,
  CREATE_THREAD,
} from './endpoints';

export default {

  // TODO parse datetimes here and return promise
  getThreads() {
    return axios.get(ALL_THREADS);
  },

  sendMessage(data) {
    let url = vsprintf(CREATE_MESSAGE, [data.threadId]);

    return axios.post(url, data);
  },

  createThread({type, id}) {
    return axios.post(CREATE_THREAD, {id: id, type});
  },

};
