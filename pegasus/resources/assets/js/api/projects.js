import axios from 'axios';
import {vsprintf} from 'sprintf-js';
import {
  ALL_PROJECTS,
  CREATE_PROJECT,
  EDIT_PROJECT,
  FIND_PROJECT_BY_ID,
} from './endpoints';

export default {

  getProject(id) {
    let url = vsprintf(FIND_PROJECT_BY_ID, [id]);

    return axios.get(url);
  },

  getProjects() {
    return axios.get(ALL_PROJECTS);
  },

  createProject(project) {
    return axios.post(CREATE_PROJECT, {
      _token: window.Laravel.csrfToken,
      ...project,
    });
  },

  editProject(id, project) {
    let url = vsprintf(EDIT_PROJECT, [id]);

    return axios.patch(url, {
      _token: window.Laravel.csrfToken,
      ...project,
    });
  },

};
