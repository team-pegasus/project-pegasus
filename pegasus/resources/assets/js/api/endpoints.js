export const LOGIN_URL = '/api/users/current/login';
export const REGISTER_URL = '/api/users/current/register';
export const LOGOUT_URL = '/api/users/current/logout';
export const PASSWORD_RESET_URL = '/api/users/current/password/email';
export const GET_USER = '/api/users/';

// messages
export const ALL_THREADS = '/api/threads';
export const CREATE_MESSAGE = '/api/threads/%s/message';
export const CREATE_THREAD = '/api/threads';
export const RECEIVE_MESSAGES = '/api/newmessages';

// projects
export const ALL_PROJECTS = '/api/projects';
export const CREATE_PROJECT = '/api/projects';
export const EDIT_PROJECT = '/api/projects/%s';
export const FIND_PROJECT_BY_ID = '/api/projects/%s';
