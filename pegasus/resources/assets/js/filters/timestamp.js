import moment from 'moment';

export default {
  ago(ts) {
    return moment.unix(ts).fromNow();
  },
};
