import VueRouter from 'vue-router';
import store from './store';
import Home from './components/pages/Home.vue';
import Login from './components/pages/Login.vue';
import Register from './components/pages/Register.vue';
import PasswordEmail from './components/pages/PasswordEmail.vue';
import UserProfile from './components/pages/UserProfile.vue';
import Messages from './components/messaging/Main.vue';
import Projects from './components/projects/Projects.vue';
import ProjectCreate from './components/projects/ProjectCreate.vue';
import ProjectShow from './components/projects/ProjectShow.vue';
import ProjectEdit from './components/projects/ProjectEdit.vue';

let routes = [
  {
    path: '/',
    component: Home,
    name: 'home',
  },
  {
    path: '/login',
    component: Login,
    name: 'login',
  },
  {
    path: '/register',
    component: Register,
    name: 'register',
  },
  {
    path: '/password/email',
    component: PasswordEmail,
    name: 'passwordResetEmail',
  },
  {
    path: '/messages',
    component: Messages,
    name: 'messages',
  },
  {
    path: '/profile/:id',
    component: UserProfile,
    name: 'userProfile',
    props: true,
  },
  {
    path: '/projects',
    component: Projects,
    name: 'projects',
  },
  {
    path: '/projects/new',
    component: ProjectCreate,
    name: 'projects.create',
  },
  {
    path: '/projects/:id',
    component: ProjectShow,
    name: 'projects.show',
  },
  {
    path: '/projects/:id/edit',
    component: ProjectEdit,
    name: 'projects.edit',
  },
];

const router = new VueRouter({
  // URLs without #
  mode: 'history',
  routes,
});

export default router;

// Routes that can't be accessed when logged in
const guestRoutes = [
  'login',
  'register',
];

// Routes that can only be accessedd when logged in
const authRoutes = [
  'messages',
  'projects.create',
  'projects.edit',
];

router.beforeEach((to, from, next) => {
  if (store.getters.loggedIn && guestRoutes.indexOf(to.name) !== -1) {
    // Redirect home if logged in user tries to visit guest route
    router.push({name: 'home'});
  } else if (!store.getters.loggedIn &&
    authRoutes.indexOf(to.name) !== -1) {
    // Redirect home (TODO Unauthorized page) if user is not
    // logged in but tries to visit auth protected route
    router.push({name: 'home'});
  } else {
    // Do nothing, just continue
    next();
  }
});
