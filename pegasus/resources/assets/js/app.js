
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import '../sass/skeleton.scss';
import Vue from 'vue';
import Vuex from 'vuex';
// Vuex store
import store from './store';
import VueRouter from 'vue-router';
import App from './App.vue';
import VueAxios from 'vue-axios';
import axios from 'axios';
import router from './routes';

window.Vue = Vue;
window.axios = axios;

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueAxios, axios);

export default new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  data: {
    loggedIn: false,
  },
  components: {App},
});
