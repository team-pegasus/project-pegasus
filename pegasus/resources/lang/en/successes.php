<?php

return [
    'db' => [
        'users' => [
            'registered' => "Successfully registered!",
        ]
    ],
    'auth' => [
        'logout' => 'Successfully logged out',
        'login'  => 'Successfully logged in'
    ]
];
