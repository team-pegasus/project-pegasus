<?php

return [
    'db'    => [
        'unknown' => 'Unknown database error'
    ],
    'auth' => "Wrong email or password",
];
