<!DOCTYPE HTML>

<html>
    <head>
        <title> </title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <!--[if lte IE 8]><script src="/css/ie/html5shiv.js"></script><![endif]-->
        <script src="/js/all_libs.js"></script>
        <!-- <script src="/vendor/bootstrap-3.3.7/js/bootstrap.min.js"></script> -->
        <!-- <script src="/js/jquery.dropotron.min.js"></script>
        <script src="/js/jquery.scrollgress.min.js"></script>
        <script src="/js/jquery.scrolly.min.js"></script>
        <script src="/js/jquery.slidertron.min.js"></script>
        <script src="/js/skel.min.js"></script>
        <script src="/js/skel-layers.min.js"></script> -->
        <script src="/js/init.js"></script>
        <noscript>
            <link rel="stylesheet" href="/css/skel.css" />
            <link rel="stylesheet" href="/css/style.css" />
            <link rel="stylesheet" href="/css/style-xlarge.css" />
        </noscript>
        <!--[if lte IE 9]><link rel="stylesheet" href="/css/ie/v9.css" /><![endif]-->
        <!--[if lte IE 8]><link rel="stylesheet" href="/css/ie/v8.css" /><![endif]-->
        <link rel="stylesheet" type="text/css" href="/vendor/bootstrap-3.3.7/css/bootstrap.min.css"/>
        @yield('head_styles')
    </head>
    <body class="landing">

        <!-- Header -->
        <!-- include('partials.master.header') -->

        @yield('content')

        <!-- Footer -->
        <footer id="footer">
            <ul class="icons">
                <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
                <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
                <li><a href="#" class="icon fa-envelope"><span class="label">Envelope</span></a></li>
            </ul>
            <ul class="menu">
                <li><a href="#">FAQ</a></li>
                <li><a href="#">Terms of Use</a></li>
                <li><a href="#">Privacy</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
            <span class="copyright">
                &copy; Copyright. All rights reserved. Design by <a href="http://www.html5webtemplates.co.uk">Responsive Web Templates</a>
            </span>
        </footer>

        <script src="/vendor/bootstrap-3.3.7/js/bootstrap.min.js"></script>
        @include('partials.master.status')
        @yield('footer_scripts')
    </body>

</html>
