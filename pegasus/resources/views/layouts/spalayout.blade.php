<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta id="meta-current-user" content='{!! auth()->check() ? auth()->user()->toJson() : '{}' !!}'>

    <title>{{ config('app.name', 'SPA') }}</title>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="TODO" />
    <meta name="keywords" content="TODO" />
    <!--[if lte IE 8]><script src="/css/ie/html5shiv.js"></script><![endif]-->
    <!-- <script src="/js/all_libs.js"></script> -->
    <!-- <link rel="stylesheet" type="text/css" href="/vendor/bootstrap-3.3.7/css/bootstrap.min.css"/> -->

    <!-- END SKEL DESIGN -->
</head>
<body class="landing">
    <div id="app">
    @yield('content')
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>
