@if((!isset($hide_status_modal) || !$hide_status_modal) && ($errors->count() > 0 || request()->session()->has('success')))
<div class="modal fade in" data-show="true" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        @include('partials.master.errors')
        @include('partials.master.success')
      </div>
      <div class="modal-footer">
        <button type="button" class="button" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">$("#statusModal").modal("show");</script>
@endif
