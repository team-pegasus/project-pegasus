<header id="header" class="@if(Route::current()->getName() == 'home') alt @endif skel-layers-fixed">
    <h1><a href="{{ route('home') }}">Pegasus <span>collaboration platform</span></a></h1>
    <nav id="nav">
        <ul>
            <li><a href="{{ route('home') }}">Home</a></li>
            <li>
                <a href="" class="icon fa-angle-down">HTML Template</a>
                <ul>
                    <li><a href="{{ route('elements.showcase') }}">Showcase</a></li>
                    <li><a href="{{ route('elements.left_sidebar') }}">Left sidebar</a></li>
                    <li><a href="{{ route('elements.right_sidebar') }}">Right sidebar</a></li>
                    <li><a href="{{ route('elements.no_sidebar') }}">No sidebar</a></li>
                </ul>
            </li>
            <li>
                @if(Auth::check())
                    <a href="" class="icon fa-angle-down">{{ Auth::user()->email}}</a>
                    <ul>
                        <li><a href="{{ route('auth.logout') }}">Log out</a></li>
                        @can('users.index')
                            <li><a href="{{ route('users.index') }}">All users</a></li>
                        @endcan
                    </ul>
                @else
                    <a href="" class="icon fa-angle-down">User</a>
                    <ul>
                        <li><a href="{{ route('auth.getRegister') }}">Register</a></li>
                        <li><a href="{{ route('auth.getLogin') }}">Log in</a></li>
                        <li><a href="{{ route('auth.getPasswordReset') }}">Forgot password</a></li>
                        <li>
                            <a href="">Submenu</a>
                            <ul>
                                <li><a href="#">Option 1</a></li>
                                <li><a href="#">Option 2</a></li>
                                <li><a href="#">Option 3</a></li>
                                <li><a href="#">Option 4</a></li>
                            </ul>
                        </li>
                    </ul>
                @endif
            </li>
        </ul>
    </nav>
</header>
