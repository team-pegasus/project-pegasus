@if(request()->session()->has('success'))
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-success">
            <ul>
                @foreach(request()->session()->get('success')->all() as $success)
                <li>{{ $success }}</li>
                @endforeach
            </ul>
    	</div>
    </div>
</div>
@endif
