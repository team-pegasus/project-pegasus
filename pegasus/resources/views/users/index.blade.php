@extends('layout.master')

@section('content')
<!-- Main -->
<section id="main" class="wrapper style1">
    <header class="major">
        {{-- <p>{{$roles}}</p> --}}
    </header>
    <div class="container">
        <!-- Content -->
        <section id="content">
            <a class="button pull-right" href="{{route('users.create')}}">New</a>
            <h2>Users</h2>
            <br/>
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Roles</th>
                        <th>Updated</th>
                        <th>Created</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->username}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            @foreach($user->roles as $role)
                                <span class="label label-default">{{$role->role}}</span>
                            @endforeach
                        </td>
                        <td>{{$user->updated_at}}</td>
                        <td>{{$user->created_at}}</td>
                        <td>
                            @can('view', $user)
                                <a href="{{route('users.show', $user->id)}}"><i class="fa fa-eye"></i></a>
                            @endcan
                            @can('update', $user)
                                <a href="{{route('users.edit', $user->id)}}"><i class="fa fa-pencil"></i></a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </section>
    </div>
</section>
@stop
