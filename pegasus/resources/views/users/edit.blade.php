@extends('layout.master')
@php($hide_status_modal=true)

@section('content')
<!-- Main -->
<section id="main" class="wrapper style1">
    <header class="major">
        {{-- <p>{{$roles}}</p> --}}
    </header>
    <div class="container">
        <!-- Content -->
        <section id="content">
            <h2>Edit {{$user->username}}</h2>

            @include('partials.master.errors')
            @include('partials.master.success')
            {{-- {{$errors->count()}} --}}
            <br/>
            {!! Form::model($user, ['method' => 'PUT', 'route' => ['users.update', $user]]) !!}
                <fieldset>
                    <div class="form-group">
                        {!! Form::text('username', old('username'), [
                            'placeholder'   => 'Username',
                            'class'         => 'form-control 9u 12u(3)',
                            'autofocus'     => ''
                        ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::text('email', old('email'), [
                            'placeholder'   => 'Email',
                            'class'         => 'form-control 9u 12u(3)',
                            'autofocus'     => ''
                        ]) !!}
                    </div>
                    <div class="form-group row">
                        @foreach($roles as $role)
                            <div class="2u 12u(3)">
                                {!! Form::checkbox('roles[]', $role, $user->hasRoles([$role]), ['id'=>'role-'.$role]) !!}
                                <label for="role-{{$role}}">
                                    {{$role}}
                                </label>
                            </div>
                        @endforeach
                    </div>


                    <!-- Change this to a button or input when using this as a form -->
                    {{-- <input type="submit" class="btn btn-lg" value="Register"/> --}}
                    {!! Form::submit('Save', ['class' => 'button']) !!}

                </fieldset>
            {!! Form::close() !!}
        </section>
    </div>
</section>
@stop
