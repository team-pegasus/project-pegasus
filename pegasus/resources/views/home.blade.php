@extends('layouts.master')

@section('content')
<!-- Banner -->
<section id="banner">
    <div class="inner">
        <h2>Pegasus</h2>
        <p>Collaboration platform for developers</p>
        <ul class="actions">
            <li><a href="#one" class="button big scrolly">Learn More</a></li>
        </ul>
    </div>
</section>

<!-- One -->
<section id="one" class="wrapper style1">
    <div class="container">
        <header class="major">
            <h2>What is Pegasus all about?</h2>
            <p>
                <br/>
                We wish to connect people to work together on mutual topics of
                interest.<br/>
                For all those who seek a hobby project to fill their weekend,
                and those who seek company<br/> on their voyage through development
                stages.
            </p>
        </header>
        <div class="slider">
            <span class="nav-previous"></span>
            <div class="viewer">
                <div class="reel">
                    <div class="slide">
                        <img src="images/slide01.jpg" alt="" />
                    </div>
                    <div class="slide">
                        <img src="images/slide02.jpg" alt="" />
                    </div>
                    <div class="slide">
                        <img src="images/slide03.jpg" alt="" />
                    </div>
                </div>
            </div>
            <span class="nav-next"></span>
        </div>
    </div>
</section>

<!-- Two -->
<section id="two" class="wrapper style2">
    <div class="container">
        <div class="row uniform">
            <div class="4u 6u(2) 12u$(3)">
                <section class="feature fa-briefcase">
                    <h3>Natoque phasellus</h3>
                    <p>Ipsum dolor tempus commodo amet sed accumsan et adipiscing blandit porttitor sed faucibus.</p>
                </section>
            </div>
            <div class="4u 6u$(2) 12u$(3)">
                <section class="feature fa-code">
                    <h3>Ultricies dolore</h3>
                    <p>Ipsum dolor tempus commodo amet sed accumsan et adipiscing blandit porttitor sed faucibus.</p>
                </section>
            </div>
            <div class="4u$ 6u(2) 12u$(3)">
                <section class="feature fa-save">
                    <h3>Magna lacinia</h3>
                    <p>Ipsum dolor tempus commodo amet sed accumsan et adipiscing blandit porttitor sed faucibus.</p>
                </section>
            </div>
            <div class="4u 6u$(2) 12u$(3)">
                <section class="feature fa-desktop">
                    <h3>Praesent lacinia</h3>
                    <p>Ipsum dolor tempus commodo amet sed accumsan et adipiscing blandit porttitor sed faucibus.</p>
                </section>
            </div>
            <div class="4u 6u(2) 12u$(3)">
                <section class="feature fa-camera-retro">
                    <h3>Morbi semper</h3>
                    <p>Ipsum dolor tempus commodo amet sed accumsan et adipiscing blandit porttitor sed faucibus.</p>
                </section>
            </div>
            <div class="4u$ 6u$(2) 12u$(3)">
                <section class="feature fa-cog">
                    <h3>Arcu consequat</h3>
                    <p>Ipsum dolor tempus commodo amet sed accumsan et adipiscing blandit porttitor sed faucibus.</p>
                </section>
            </div>
        </div>
    </div>
</section>

<!-- Three -->
<section id="three" class="wrapper style1">
    <div class="container">
        <header class="major">
            <h2>Highlights</h2>
            <p>Here are some staff-picked project we wish would succeed.</p>
        </header>
        <div class="row">
            <div class="4u 6u(2) 12u$(3)">
                <article class="box post">
                    <a href="#" class="image fit"><img src="images/pic01.jpg" alt="" /></a>
                    <h3>Change the world</h3>
                    <p>Ipsum dolor tempus et commodo lorem accumsan et adipiscing blandit porttitor feugiat tempus lorem faucibus.</p>
                    <ul class="actions">
                        <li><a href="#" class="button">Learn More</a></li>
                    </ul>
                </article>
            </div>
            <div class="4u 6u$(2) 12u$(3)">
                <article class="box post">
                    <a href="#" class="image fit"><img src="images/pic02.jpg" alt="" /></a>
                    <h3>Movie library</h3>
                    <p>Ipsum dolor tempus et commodo lorem accumsan et adipiscing blandit porttitor feugiat tempus lorem faucibus.</p>
                    <ul class="actions">
                        <li><a href="#" class="button">Learn More</a></li>
                    </ul>
                </article>
            </div>
            <div class="4u$ 6u(2) 12u$(3)">
                <article class="box post">
                    <a href="#" class="image fit"><img src="images/pic03.jpg" alt="" /></a>
                    <h3>3D game engine</h3>
                    <p>Ipsum dolor tempus et commodo lorem accumsan et adipiscing blandit porttitor feugiat tempus lorem faucibus.</p>
                    <ul class="actions">
                        <li><a href="#" class="button">Learn More</a></li>
                    </ul>
                </article>
            </div>
            <div class="4u 6u$(2) 12u$(3)">
                <article class="box post">
                    <a href="#" class="image fit"><img src="images/pic04.jpg" alt="" /></a>
                    <h3>Web security training website</h3>
                    <p>Ipsum dolor tempus et commodo lorem accumsan et adipiscing blandit porttitor feugiat tempus lorem faucibus.</p>
                    <ul class="actions">
                        <li><a href="#" class="button">Learn More</a></li>
                    </ul>
                </article>
            </div>
            <div class="4u 6u(2) 12u$(3)">
                <article class="box post">
                    <a href="#" class="image fit"><img src="images/pic05.jpg" alt="" /></a>
                    <h3>Tinder for cars</h3>
                    <p>Ipsum dolor tempus et commodo lorem accumsan et adipiscing blandit porttitor feugiat tempus lorem faucibus.</p>
                    <ul class="actions">
                        <li><a href="#" class="button">Learn More</a></li>
                    </ul>
                </article>
            </div>
            <div class="4u$ 6u$(2) 12u$(3)">
                <article class="box post">
                    <a href="#" class="image fit"><img src="images/pic06.jpg" alt="" /></a>
                    <h3>Web plug-in for people with disabilities</h3>
                    <p>Ipsum dolor tempus et commodo lorem accumsan et adipiscing blandit porttitor feugiat tempus lorem faucibus.</p>
                    <ul class="actions">
                        <li><a href="#" class="button">Learn More</a></li>
                    </ul>
                </article>
            </div>
        </div>
    </div>
</section>

<!-- CTA -->
<section id="cta" class="wrapper style3">
    <h2>Create your own project</h2>
    <ul class="actions">
        <li><a href="#" class="button big">Get Started</a></li>
    </ul>
</section>
>>>>>>> develop

@stop
