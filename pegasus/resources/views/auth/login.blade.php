@extends('layouts.master')
@php($hide_status_modal=true)

@section('content')
<section id="main" class="wrapper style1">
    <header class="major">
        <h2>Sign in</h2>
        <!-- <p>Create a new account</p> -->
    </header>
    <div class="container">
        @include('partials.master.errors')
        @include('partials.master.success')

        <div class="row 150%">
            <div class="6u 12u$(2)">
            <!-- Content -->
                <section id="content">
                    <h3>Your account information</h3>
                    {!! Form::open(['method' => 'POST', 'route' => 'auth.postLogin']) !!}
                        <fieldset>
                            <div class="form-group">
                                {!! Form::text('email', old('email'), [
                                    'placeholder'   => 'Email',
                                    'class'         => 'form-control',
                                    'autofocus'     => ''
                                ]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::password('password', [
                                    'placeholder'   => 'Password',
                                    'class'         => 'form-control',
                                ]) !!}
                            </div>

                            <!-- Change this to a button or input when using this as a form -->
                            <input type="submit" class="btn btn-lg" value="Log in">
                        </fieldset>
                    {!! Form::close() !!}
                </section>
            </div>
            <div class="6u 12u$(2)">
            <!-- Content -->
                <section>
                    <h3>Login with provider</h3>
                    <ul class="icons">
                        <li>
                            <a title='google' href="{{ route('auth.oauth', 'google') }}" class="icon fa-google"><span class="label">Google</span></a>
                        </li>
                        <li>
                            <a title='github' href="{{ route('auth.oauth', 'github') }}" class="icon fa-github"><span class='label'>Github</span></a>
                        </li>
                        <li>
                            <a title='bitbucket' href="{{ route('auth.oauth', 'bitbucket') }}" class="icon fa-bitbucket"><span class='label'>Bitbucket</span></a>
                        </li>
                    </ul>
                </section>
            </div>
        </div>
    </div>
</section>

@stop
