@extends('layout.master')
@php($hide_status_modal=true)

@section('content')
<section id="main" class="wrapper style1">
    <header class="major">
        <h2>Password reset</h2>
        <!-- <p>Create a new account</p> -->
    </header>
    <div class="container">
        @include('partials.master.errors')
        @include('partials.master.success')

        <!-- Content -->
        <section id="content">
            <h3>New password</h3>
            {!! Form::open(['method' => 'POST', 'route' => 'auth.postPasswordReset']) !!}
                <fieldset>

                    {!! Form::hidden("token", $token) !!}

                    <div class="form-group">
                        {!! Form::text('email', old('email'), [
                            'placeholder'   => 'Email',
                            'class'         => 'form-control',
                            'autofocus'     => ''
                        ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::password('password', [
                            'placeholder'   => 'Password',
                            'class'         => 'form-control',
                        ]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::password('password_confirmation', [
                            'placeholder'   => 'Password confirmation',
                            'class'         => 'form-control',
                        ]) !!}
                    </div>

                    <!-- Change this to a button or input when using this as a form -->
                    <input type="submit" class="btn btn-lg" value="Log in">
                </fieldset>
            {!! Form::close() !!}
        </section>
    </div>
</section>

@stop
