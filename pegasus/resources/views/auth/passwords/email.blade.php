@extends('layout.master')
@php($hide_status_modal=true)

@section('content')
<section id="main" class="wrapper style1">
    <header class="major">
        <h2>Password reset</h2>
        <!-- <p>Create a new account</p> -->
    </header>
    <div class="container">
        @include('partials.master.errors')
        @include('partials.master.success')

        <!-- Content -->
        <section id="content">
            <h3>Your email</h3>
            {!! Form::open(['method' => 'POST', 'route' => 'auth.postPasswordResetEmail']) !!}
                <fieldset>

                    <div class="form-group">
                        {!! Form::text('email', old('email'), [
                            'placeholder'   => 'Email',
                            'class'         => 'form-control',
                            'autofocus'     => ''
                        ]) !!}
                    </div>

                    <!-- Change this to a button or input when using this as a form -->
                    <input type="submit" class="btn btn-lg" value="Log in">
                </fieldset>
            {!! Form::close() !!}
        </section>
    </div>
</section>

@stop
