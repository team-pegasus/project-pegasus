# Project Pegasus
A collaboration platform made by developers for developers

## Setup
Depends on `composer`, `php7`,  `mysql`.

```
$ git clone git@gitlab.com:team-pegasus/project-pegasus.git
$ cd project-pegasus/pegasus
$ mysql
> CREATE DATABASE pegasus;
> COMMIT;
> exit;
$ cp .env.example .env
$ nano .env  # set up the environment variables
$ composer install
$ php artisan migrate
$ php artisan serve
```

### Frontend building
All `.sass` and `.js` files are located in `pegasus/resources/assets`. All
files to be build are defined in `pegasus/webpack.mix.js`.

Use `npm run dev` to generate final `css` and `js` files or `npm run production`
to generate minimized versions of files.

## Features
* User registration/login
  * On-site
  * OAuth2 using: GitHub, Google, Bitbucket
* Create/browse projects

## Coding style

Coding style used is PSR-2 as defied [here](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md).

### Naming convention
* All class names MUST have `PascalCase`.
* Class properties, methods, as well as standalone functions and variables MUST be in `camelCase`.
* Constants MUST be `UPPER_UNDERSCORE`.

# Features

## ACL
The system has statically defined roles in `config/roles.php`. Each user can have multiple roles (via `users_roles` table).

Every role has a defined set of permissions, which are used to grant or deny access to certain features on the site.
